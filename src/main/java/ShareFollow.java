import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import common.ColumnEnum;
import common.FileUtils;
import common.FollowUtils;
import common.TableCellListener;
import entity.MaEntity;
import entity.ShareEntity;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.time.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import static common.TimeConstant.*;

public class ShareFollow extends JFrame {
    public JPanel panel1;
    //服务端ip地址和端口号
    public String ipAndPort;
    private JScrollPane scrollPane1;
    private JButton startButton;
    private JButton openButton;
    private JButton clearButton;
    //文件路径
    public String filePath;
    //新浪实时请求
    private JTable table1;
    private JButton saveButton;
    private JPopupMenu jPopupMenu;
    private DefaultTableModel defaultTableModel;
    private JLabel timeLabel;
    private TrayIcon trayIcon;
    private JLabel maLabel;
    private JComboBox comboBox1;
    private JCheckBox fileCheckBox;
    private JCheckBox drawSelfCheckBox;
    private JCheckBox poolCheckBox;
    private JButton addButton;
    private JButton removeButton;
    private JButton upButton;
    private JButton downButton;
    private JCheckBox greenFilterCheckBox;
    private JCheckBox redFilterCheckBox;
    //系统托盘
    private SystemTray tray;
    private HttpGet realHttpGet;
    private ScheduledFuture<?> realFuture;
    private ScheduledFuture<?> viewFuture;
    private ScheduledFuture<?> maFuture;
    private ScheduledFuture<?> volFuture;
    private CloseableHttpClient closeableHttpClient = HttpClients.createDefault();
    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
    private ScheduledFuture<?> deviationFuture;
    private ScheduledFuture<?> realMaFuture;
    //股票列表
    private List<ShareEntity> shareEntityList = new ArrayList<>();
    //告警是否可用
    private Boolean warnEnable = Boolean.TRUE;
    //前一个交易日期
    private LocalDate yesterday = null;
    //最近一个交易日期
    private LocalDate today = null;
    //区间涨跌幅起始日期
    private LocalDate startDate = null;
    //最近一个交易日期的9点30分
    private LocalDateTime nine30 = null;
    //最近一个交易日期的11点30分
    private LocalDateTime eleven30 = null;
    //最近一个交易日期的13点00分
    private LocalDateTime thirteen = null;
    //最近一个交易日期的15点00分
    private LocalDateTime fifteen = null;
    //前一天各时刻累计成交量缓存
    private Map<String, double[]> yesRealVolCache = new HashMap<>();
    //日线收盘价缓存
    private Map<String, double[]> dayCloseCache = new HashMap<>();
    //日线成交量缓存
    private Map<String, double[]> dayVolCache = new HashMap<>();
    //周线收盘价缓存
    private Map<String, double[]> weekCloseCache = new HashMap<>();
    //月线收盘价缓存
    private Map<String, double[]> monthCloseCache = new HashMap<>();
    private Pair<Double, Double> g1Market = null;
    private Pair<Double, Double> g2Market = null;
    private Pair<Double, Double> g1Share = null;
    private Pair<Double, Double> g2Share = null;
    private Pair<Double, Double> g3Share = null;
    private Pair<Double, Double> v20Threshold = null;
    private Boolean futureMode = false;
    //期货代码类型：fi、main、sec
    private String futureCodeType = null;
    private Double min1Threshold = null;
    //上涨家数
    private Integer upCount = 0;
    //下跌家数
    private Integer downCount = 0;
    //两市总成交额
    private Double aAmount = 0.0;
    private Map<String, Double> dayMa20 = new HashMap<>();
    private Map<String, Double> weekMa20 = new HashMap<>();
    private Map<String, Double> monthMa20 = new HashMap<>();

    public ShareFollow() {
        try {
            Class.forName("MyHttpServer");
            setTitle("ShareFollow");
            setContentPane(panel1);
            setVisible(true);
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            Integer width = 1130;
            Integer height = 300;
            setMinimumSize(new Dimension(width, height));
            Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
            int x = (int) ((dimension.getWidth() - width) / 2);
            int y = (int) ((dimension.getHeight() - height) / 2);
            this.setLocation(x, y);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        addButton.setVisible(false);
        removeButton.setVisible(false);
        upButton.setVisible(false);
        downButton.setVisible(false);
        //打开文件按钮事件监听器
        openButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FileDialog fileDialog = new FileDialog(new Frame(), "打开txt文件", FileDialog.LOAD);
                fileDialog.setDirectory("/");
                fileDialog.setFile("*.txt");
                fileDialog.setVisible(true);
                //解析文件
                if (null != fileDialog.getFile()) {
                    defaultTableModel.setRowCount(0);
                    shareEntityList.clear();
                    Integer count = 1;
                    filePath = fileDialog.getDirectory() + fileDialog.getFile();
                    //解析股票代码列表
                    try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"))) {
                        String line = null;
                        List<ShareEntity> list = new ArrayList<>();
                        while (StringUtils.isNotBlank(line = bufferedReader.readLine())) {
                            String[] arr = line.split(",");
                            ShareEntity shareEntity = new ShareEntity();
                            String code = arr[0];
                            Boolean isEastFutureCode = isEastFutureCode(code);
                            if (!code.startsWith("sh") && !code.startsWith("sz") && !isEastFutureCode) {
                                timeLabel.setText(String.format("第%d行代码格式错误!", count));
                                startButton.setEnabled(false);
                                saveButton.setEnabled(false);
                                clearButton.setEnabled(false);
                                return;
                            }
                            String shareCode = null;
                            String sinaCode = null;
                            String eastCode = null;
                            if (isEastFutureCode) {
                                shareCode = code;
                                sinaCode = code;
                                eastCode = code;
                                shareEntity.setShareName(arr[1]);
                                if (shareEntity.getShareName().contains("加权")) {
                                    futureCodeType = "fi";
                                } else if (shareEntity.getShareName().contains("次主")) {
                                    futureCodeType = "sec";
                                } else {
                                    futureCodeType = "main";
                                }
                                futureMode = true;
                            }
                            //股票代码强制转为小写
                            else {
                                sinaCode = code.toLowerCase();
                                shareCode = sinaCode.substring(2);
                                eastCode = sinaCode.startsWith("sz") ? "0." + shareCode : "1." + shareCode;
                                futureMode = false;
                                futureCodeType = null;
                            }
                            shareEntity.setShareCode(shareCode);
                            shareEntity.setSinaCode(sinaCode);
                            shareEntity.setEastCode(eastCode);
                            shareEntity.setMaEntity(new MaEntity());
                            if (arr.length > 2) {
                                shareEntity.setRemark(arr[2]);
                            }
                            list.add(shareEntity);
                            count++;
                        }
                        shareEntityList.addAll(list);
                        bufferedReader.close();
                        timeLabel.setText("导入成功!");
                    } catch (Exception e1) {
                        timeLabel.setText(String.format("第%d行数据解析失败!", count));
                        startButton.setEnabled(false);
                        saveButton.setEnabled(false);
                        clearButton.setEnabled(false);
                        e1.printStackTrace();
                        return;
                    }
                    Integer size = shareEntityList.size();
                    defaultTableModel.setRowCount(size);
                    for (Integer i = 0; i < size; i++) {
                        ShareEntity shareEntity = shareEntityList.get(i);
                        defaultTableModel.setValueAt(String.format("%03d", i + 1), i, ColumnEnum.TABLE_NO.getCode());
                        defaultTableModel.setValueAt(shareEntity.getSinaCode(), i, ColumnEnum.TABLE_MC.getCode());
                        defaultTableModel.setValueAt(shareEntity.getRemark(), i, ColumnEnum.TABLE_BZ.getCode());
                    }
                    if (futureMode) {
                        realHttpGet = new HttpGet(String.format("https://push2.eastmoney.com/api/qt/ulist.np/get?secids=%s&fields=f14,f15,f16,f1,f2,f5,f6,f17", StringUtils.join(shareEntityList.stream().map(ShareEntity::getEastCode).collect(Collectors.toList()), ",")));
                        //设置超时时间为1000ms
                        realHttpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                    } else {
                        realHttpGet = new HttpGet("http://hq.sinajs.cn/list=" + StringUtils.join(shareEntityList.stream().map(ShareEntity::getSinaCode).collect(Collectors.toList()), ","));
                        //设置超时时间为1000ms
                        realHttpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                        realHttpGet.addHeader("Referer", "https://finance.sina.com.cn");
                    }
                    ImmutablePair<LocalDate, LocalDate> immutablePair = FollowUtils.queryYesterdayAndToday(closeableHttpClient, futureMode);
                    yesterday = immutablePair.getLeft();
                    today = immutablePair.getRight();
                    startButton.setEnabled(true);
                    //支持排序
                    table1.setRowSorter(FollowUtils.getColumnSorters(defaultTableModel));
                    //加载配置
                    String property = FileUtils.getConfig("ipAndPort");
                    if (StringUtils.isNotEmpty(property)) {
                        ipAndPort = property;
                    } else {
                        ipAndPort = "127.0.0.1:1234";
                    }
                    property = FileUtils.getConfig("marketThreshold");
                    if (StringUtils.isNotEmpty(property)) {
                        String[] arr = property.split(";");
                        String[] arrF1 = arr[0].split("-");
                        String[] arrF2 = arr[1].split("-");
                        g1Market = Pair.of(Double.valueOf(arrF1[0]), Double.valueOf(arrF1[1]));
                        g2Market = Pair.of(Double.valueOf(arrF2[0]), Double.valueOf(arrF2[1]));
                    }
                    property = FileUtils.getConfig("shareThreshold");
                    if (StringUtils.isNotEmpty(property)) {
                        String[] arr = property.split(";");
                        String[] arrF1 = arr[0].split("-");
                        String[] arrF2 = arr[1].split("-");
                        String[] arrF3 = arr[2].split("-");
                        g1Share = Pair.of(Double.valueOf(arrF1[0]), Double.valueOf(arrF1[1]));
                        g2Share = Pair.of(Double.valueOf(arrF2[0]), Double.valueOf(arrF2[1]));
                        g3Share = Pair.of(Double.valueOf(arrF3[0]), Double.valueOf(arrF3[1]));
                    }
                    property = FileUtils.getConfig("v20Threshold");
                    if (StringUtils.isNotEmpty(property)) {
                        String[] arr = property.split("-");
                        v20Threshold = Pair.of(Double.valueOf(arr[0]), Double.valueOf(arr[1]));
                    }
                    initPopupMenuList();
                }
            }
        });
        //开始按钮事件监听器
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //启动
                try {
                    if (startButton.getText().equals("Start")) {
                        start();
                    }
                    //停止
                    else {
                        stop();
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        //保存按钮事件监听器
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int rs = JOptionPane.showOptionDialog(null, "请选择k线级别", "选择", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                        FollowUtils.KLINE_LEVEL, FollowUtils.KLINE_LEVEL[0]);
                if (rs < 0) {
                    return;
                }
                String kLineLevel = FollowUtils.KLINE_LEVEL[rs];
                Integer size = table1.getRowCount();
                new Thread() {
                    @Override
                    public void run() {
                        String path = String.format("F:\\%s_%s\\", new File(filePath).getName().split("\\.")[0], comboBox1.getSelectedItem());
                        FileUtils.deleteDirectory(path);
                        FileUtils.createDirectory(path);
                        for (Integer idx = 0; idx < size; idx++) {
                            Integer count = 3;
                            while (count-- > 0) {
                                HttpGet httpGet = null;
                                try {
                                    ShareEntity shareEntity = shareEntityList.get(Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1);
                                    String code = shareEntity.getShareCode();
                                    String name = shareEntity.getShareName();
                                    if (kLineLevel.equals("日线")) {
                                        if (drawSelfCheckBox.isSelected()) {
                                            httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=day&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                                                    shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2)));
                                            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                                        } else {
                                            httpGet = new HttpGet(String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=D",
                                                    shareEntity.getEastCode()));
                                            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(3000).build());
                                        }
                                        File file = new File(path + String.format("%03d-%s-%s-%s.png", idx, code, name, "日线"));
                                        byte[] bytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                                        if (!drawSelfCheckBox.isSelected()) {
                                            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
                                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                                            if (addTextWatermark(inputStream, outputStream, shareEntity.getEastCode(), "day", (String) defaultTableModel.getValueAt(Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1, ColumnEnum.TABLE_BZ.getCode()))) {
                                                bytes = outputStream.toByteArray();
                                            }
                                            inputStream.close();
                                            outputStream.close();
                                        }
                                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                                        fileOutputStream.write(bytes);
                                        fileOutputStream.close();
                                    } else if (kLineLevel.equals("周线")) {
                                        if (drawSelfCheckBox.isSelected()) {
                                            httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=week&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                                                    shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2)));
                                            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                                        } else {
                                            httpGet = new HttpGet(String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=W",
                                                    shareEntity.getEastCode()));
                                            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(3000).build());
                                        }
                                        File file = new File(path + String.format("%03d-%s-%s-%s.png", idx, code, name, "周线"));
                                        byte[] bytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                                        if (!drawSelfCheckBox.isSelected()) {
                                            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
                                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                                            if (addTextWatermark(inputStream, outputStream, shareEntity.getEastCode(), "week", null)) {
                                                bytes = outputStream.toByteArray();
                                            }
                                            inputStream.close();
                                            outputStream.close();
                                        }
                                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                                        fileOutputStream.write(bytes);
                                        fileOutputStream.close();
                                    } else if (kLineLevel.equals("日周线")) {
                                        String dayUrl = null;
                                        String weekUrl = null;
                                        if (drawSelfCheckBox.isSelected()) {
                                            dayUrl = String.format("http://%s/kline?code=%s&name=%s&today=%s&period=day&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(), shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2));
                                            weekUrl = String.format("http://%s/kline?code=%s&name=%s&today=%s&period=week&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(), shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2));
                                        } else {
                                            dayUrl = String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=D", shareEntity.getEastCode());
                                            weekUrl = String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=W", shareEntity.getEastCode());
                                        }
                                        File file = new File(path + String.format("%03d-%s-%s-%s.png", idx + 1, code, name, "日周线"));
                                        FollowUtils.mergeImage(Arrays.asList(new ByteArrayInputStream(getSharePicture(dayUrl, shareEntity.getEastCode(), "day", idx)),
                                                new ByteArrayInputStream(getSharePicture(weekUrl, shareEntity.getEastCode(), "week", idx))),
                                                file, false);
                                    } else if (kLineLevel.equals("月线")) {
                                        if (drawSelfCheckBox.isSelected()) {
                                            httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=month&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                                                    shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2)));
                                            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                                        } else {
                                            httpGet = new HttpGet(String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=M",
                                                    shareEntity.getEastCode()));
                                            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(3000).build());
                                        }
                                        File file = new File(path + String.format("%03d-%s-%s-%s.png", idx, code, name, "月线"));
                                        byte[] bytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                                        if (!drawSelfCheckBox.isSelected()) {
                                            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
                                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                                            if (addTextWatermark(inputStream, outputStream, shareEntity.getEastCode(), "month", null)) {
                                                bytes = outputStream.toByteArray();
                                            }
                                            inputStream.close();
                                            outputStream.close();
                                        }
                                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                                        fileOutputStream.write(bytes);
                                        fileOutputStream.close();
                                    } else if (kLineLevel.equals("日周月线")) {
                                        String dayUrl = null;
                                        String weekUrl = null;
                                        String monthUrl = null;
                                        if (drawSelfCheckBox.isSelected()) {
                                            dayUrl = String.format("http://%s/kline?code=%s&name=%s&today=%s&period=day&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(), shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2));
                                            weekUrl = String.format("http://%s/kline?code=%s&name=%s&today=%s&period=week&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(), shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2));
                                            monthUrl = String.format("http://%s/kline?code=%s&name=%s&today=%s&period=month&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(), shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2));
                                        } else {
                                            dayUrl = String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=D", shareEntity.getEastCode());
                                            weekUrl = String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=W", shareEntity.getEastCode());
                                            monthUrl = String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=M", shareEntity.getEastCode());
                                        }
                                        File file = new File(path + String.format("%03d-%s-%s-%s.png", idx + 1, code, name, "日周月线"));
                                        FollowUtils.mergeImage(Arrays.asList(new ByteArrayInputStream(getSharePicture(dayUrl, shareEntity.getEastCode(), "day", idx)),
                                                new ByteArrayInputStream(getSharePicture(weekUrl, shareEntity.getEastCode(), "week", idx)),
                                                new ByteArrayInputStream(getSharePicture(monthUrl, shareEntity.getEastCode(), "month", idx))),
                                                file, false);
                                    }
                                    break;
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                    //休眠5秒
                                    FollowUtils.mySleep(5 * 1000);
                                } finally {
                                    if (Objects.nonNull(httpGet)) {
                                        httpGet.releaseConnection();
                                    }
                                }
                            }
                        }
                    }
                }.start();
            }
        });
        //清空告警按钮事件监听器
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Integer idx = table1.getSelectedRow();
                if (idx >= 0) {
                    table1.setValueAt(null, idx, ColumnEnum.TABLE_SYZ.getCode());
                    table1.setValueAt(null, idx, ColumnEnum.TABLE_XYZ.getCode());
                }
            }
        });
        //添加按钮事件监听器
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (futureMode) {
                    return;
                }
                Integer idx = table1.getSelectedRow();
                if (idx >= 0) {
                    String text = JOptionPane.showInputDialog("请输入代码或者名称:", null);
                    if (StringUtils.isBlank(text)) {
                        return;
                    }
                    String sinaCode = null;
                    String name = null;
                    HttpGet httpGet = null;
                    try {
                        httpGet = new HttpGet(String.format("https://suggest3.sinajs.cn/suggest/?key=%s", text));
                        httpGet.addHeader("Referer", "https://finance.sina.com.cn");
                        httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                        String rs = EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity());
                        String[] arr = rs.split(";")[0].split(",");
                        sinaCode = arr[3];
                        name = arr[4];
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    } finally {
                        if (Objects.nonNull(httpGet)) {
                            httpGet.releaseConnection();
                        }
                    }
                    //已有则跳过
                    if (StringUtils.join(shareEntityList.stream().map(ShareEntity::getSinaCode).collect(Collectors.toList()), "").contains(sinaCode)) {
                        return;
                    }
                    if (StringUtils.isNotBlank(sinaCode) && StringUtils.isNotBlank(name)) {
                        if (startButton.getText().equals("Stop")) {
                            stop();
                        }
                        ShareEntity shareEntity = new ShareEntity();
                        shareEntity.setSinaCode(sinaCode);
                        shareEntity.setShareName(name);
                        shareEntity.setShareCode(sinaCode.substring(2));
                        shareEntity.setEastCode(sinaCode.startsWith("sz") ? "0." + sinaCode.substring(2) : "1." + sinaCode.substring(2));
                        shareEntity.setMaEntity(new MaEntity());
                        shareEntityList.add(idx + 1, shareEntity);
                        defaultTableModel.insertRow(idx + 1, new Object[]{});
                        Integer size = shareEntityList.size();
                        for (Integer i = idx + 1; i < size; i++) {
                            ShareEntity item = shareEntityList.get(i);
                            defaultTableModel.setValueAt(String.format("%03d", i + 1), i, ColumnEnum.TABLE_NO.getCode());
                            defaultTableModel.setValueAt(StringUtils.isNotBlank(item.getShareName()) ? item.getShareName() + item.getShareCode() : item.getSinaCode(), i, ColumnEnum.TABLE_MC.getCode());
                        }
                        realHttpGet.setURI(URI.create("http://hq.sinajs.cn/list=" + StringUtils.join(shareEntityList.stream().map(ShareEntity::getSinaCode).collect(Collectors.toList()), ",")));
                        saveToFile();
                    }
                }
            }
        });
        //移除按钮事件监听器
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (futureMode) {
                    return;
                }
                Integer idx = table1.getSelectedRow();
                if (idx >= 0) {
                    if (startButton.getText().equals("Stop")) {
                        stop();
                    }
                    shareEntityList.remove(idx.intValue());
                    defaultTableModel.removeRow(idx.intValue());
                    Integer size = shareEntityList.size();
                    for (Integer i = idx; i < size; i++) {
                        ShareEntity item = shareEntityList.get(i);
                        defaultTableModel.setValueAt(String.format("%03d", i + 1), i, ColumnEnum.TABLE_NO.getCode());
                        defaultTableModel.setValueAt(StringUtils.isNotBlank(item.getShareName()) ? item.getShareName() + item.getShareCode() : item.getSinaCode(), i, ColumnEnum.TABLE_MC.getCode());
                    }
                    realHttpGet.setURI(URI.create("http://hq.sinajs.cn/list=" + StringUtils.join(shareEntityList.stream().map(ShareEntity::getSinaCode).collect(Collectors.toList()), ",")));
                    saveToFile();
                }
            }
        });
        //上移按钮事件监听器
        upButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (futureMode) {
                    return;
                }
                Integer idx = table1.getSelectedRow();
                if (idx > 0) {
                    if (startButton.getText().equals("Stop")) {
                        stop();
                    }
                    Collections.swap(shareEntityList, idx, idx - 1);
                    Object obj1 = defaultTableModel.getDataVector().get(idx);
                    Object obj0 = defaultTableModel.getDataVector().get(idx - 1);
                    defaultTableModel.getDataVector().set(idx, obj0);
                    defaultTableModel.getDataVector().set(idx - 1, obj1);
                    realHttpGet.setURI(URI.create("http://hq.sinajs.cn/list=" + StringUtils.join(shareEntityList.stream().map(ShareEntity::getSinaCode).collect(Collectors.toList()), ",")));
                    defaultTableModel.fireTableDataChanged();
                    saveToFile();
                }
            }
        });
        //下移按钮事件监听器
        downButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (futureMode) {
                    return;
                }
                Integer idx = table1.getSelectedRow();
                if (idx >= 0 && idx < shareEntityList.size() - 1) {
                    if (startButton.getText().equals("Stop")) {
                        stop();
                    }
                    Collections.swap(shareEntityList, idx, idx + 1);
                    Object obj0 = defaultTableModel.getDataVector().get(idx);
                    Object obj1 = defaultTableModel.getDataVector().get(idx + 1);
                    defaultTableModel.getDataVector().set(idx, obj1);
                    defaultTableModel.getDataVector().set(idx + 1, obj0);
                    realHttpGet.setURI(URI.create("http://hq.sinajs.cn/list=" + StringUtils.join(shareEntityList.stream().map(ShareEntity::getSinaCode).collect(Collectors.toList()), ",")));
                    defaultTableModel.fireTableDataChanged();
                    saveToFile();
                }
            }
        });
        //表格快捷键
        table1.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_F1) {
                    displayFSPicture();
                } else if (e.getKeyCode() == KeyEvent.VK_F2) {
                    displayRKPicture();
                } else if (e.getKeyCode() == KeyEvent.VK_F3) {
                    displayGK();
                } else if (e.getKeyCode() == KeyEvent.VK_F4) {
                    displayRZKPicture();
                } else if (e.getKeyCode() == KeyEvent.VK_F5) {
                    displayRZYKPicture(null);
                } else if (e.getKeyCode() == KeyEvent.VK_F6) {
                    if (poolCheckBox.isSelected()) {
                        updatePool();
                    } else {
                        Integer idx = table1.getSelectedRow();
                        if (idx >= 0) {
                            ShareEntity shareEntity = shareEntityList.get((Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1));
                            displayRefRZYPicture(shareEntity.getEastCode());
                        }
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_F7) {
                    if (fileCheckBox.isSelected()) {
                        addButton.doClick();
                    } else {
                        displayMarketKPicture("ths");
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_F8) {
                    if (fileCheckBox.isSelected()) {
                        removeButton.doClick();
                    } else {
                        displayMarketKPicture("east");
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_F9) {
                    if (fileCheckBox.isSelected()) {
                        upButton.doClick();
                    } else {
                        displayMarketKPicture("sw");
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_F10) {
                    if (fileCheckBox.isSelected()) {
                        downButton.doClick();
                    }
                } else if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_D) {
                    //隐藏表格
                    table1.setVisible(false);
                    table1.getTableHeader().setVisible(false);
                    maLabel.setVisible(false);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        //行选中事件监听器
        table1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    Integer idx = table1.getSelectedRow();
                    if (idx >= 0) {
                        ShareEntity shareEntity = shareEntityList.get(Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1);
                        Double yesClose = shareEntity.getYesClose();
                        Double highPercent = null;
                        Double lowPercent = null;
                        Double avgPrice = null;
                        Double avgPercent = null;
                        if (null != yesClose) {
                            highPercent = (shareEntity.getHighPrice() - yesClose) / yesClose * 100;
                            lowPercent = (shareEntity.getLowPrice() - yesClose) / yesClose * 100;
                            if (futureMode) {
                                avgPrice = (shareEntity.getHighPrice() + shareEntity.getLowPrice()) / 2;
                            } else {
                                avgPrice = shareEntity.getTodayRealAmt() / shareEntity.getTodayRealVol();
                            }
                            avgPercent = (avgPrice - yesClose) / yesClose * 100;
                        }
                        MaEntity maEntity = shareEntity.getMaEntity();
                        if ("sh000001".equals(shareEntity.getSinaCode())) {
                            maLabel.setText(String.format("H:%.2f%%,%.2f   L:%.2f%%,%.2f   ma10:%.2f   ma20:%.2f   %d/%d %.2f万亿",
                                    highPercent,
                                    shareEntity.getHighPrice(),
                                    lowPercent,
                                    shareEntity.getLowPrice(),
                                    maEntity.getMa10(),
                                    maEntity.getMa20(),
                                    upCount,
                                    downCount,
                                    aAmount / 1e12));
                        } else {
                            maLabel.setText(String.format("H:%.2f%%   L:%.2f%%   A:%.2f%%,%.3f   ma10:%.3f   ma20:%.3f",
                                    highPercent,
                                    lowPercent,
                                    avgPercent,
                                    avgPrice,
                                    maEntity.getMa10(),
                                    maEntity.getMa20()));
                        }
                    }
                }
            }
        });
        //下拉列表过滤
        for (String key : FollowUtils.COMBOBOX_MAP.keySet()) {
            comboBox1.addItem(key);
        }
        comboBox1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    if (startButton.getText().equals("Start")) {
                        String cur = (String) comboBox1.getSelectedItem();
                        if (cur.contains("周线") && !cur.contains("日")) {
                            for (Integer idx = 0; idx < defaultTableModel.getRowCount(); idx++) {
                                ShareEntity shareEntity = shareEntityList.get(idx);
                                defaultTableModel.setValueAt(null != weekMa20.get(shareEntity.getShareCode()) ? String.format("%.2f%%", weekMa20.get(shareEntity.getShareCode())) : null, idx, ColumnEnum.TABLE_G20.getCode());
                            }
                        } else if (cur.contains("月线") && !cur.contains("周")) {
                            for (Integer idx = 0; idx < defaultTableModel.getRowCount(); idx++) {
                                ShareEntity shareEntity = shareEntityList.get(idx);
                                defaultTableModel.setValueAt(null != monthMa20.get(shareEntity.getShareCode()) ? String.format("%.2f%%", monthMa20.get(shareEntity.getShareCode())) : null, idx, ColumnEnum.TABLE_G20.getCode());
                            }
                        } else {
                            for (Integer idx = 0; idx < defaultTableModel.getRowCount(); idx++) {
                                ShareEntity shareEntity = shareEntityList.get(idx);
                                defaultTableModel.setValueAt(null != dayMa20.get(shareEntity.getShareCode()) ? String.format("%.2f%%", dayMa20.get(shareEntity.getShareCode())) : null, idx, ColumnEnum.TABLE_G20.getCode());
                            }
                        }
                    }
                    refreshRowFilter();
                }
            }
        });
        Action action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TableCellListener tcl = (TableCellListener) e.getSource();
                if (ColumnEnum.TABLE_BZ.getCode().equals(tcl.getColumn()) && !StringUtils.equals((String) tcl.getOldValue(), (String) tcl.getNewValue())) {
                    String tag = (String) tcl.getNewValue();
                    Integer idx = tcl.getRow();
                    shareEntityList.get(idx).setRemark(tag);
                    saveToFile();
                }
            }
        };
        TableCellListener tcl = new TableCellListener(table1, action);
        fileCheckBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (!table1.isVisible()) {
                    table1.setVisible(true);
                    table1.getTableHeader().setVisible(true);
                    maLabel.setVisible(true);
                }
                if (futureMode) {
                    return;
                }
                if (fileCheckBox.isSelected()) {
                    addButton.setVisible(true);
                    removeButton.setVisible(true);
                    upButton.setVisible(true);
                    downButton.setVisible(true);
                    table1.setRowSorter(null);
                } else {
                    addButton.setVisible(false);
                    removeButton.setVisible(false);
                    upButton.setVisible(false);
                    downButton.setVisible(false);
                    table1.setRowSorter(FollowUtils.getColumnSorters(defaultTableModel));
                }
            }
        });
        poolCheckBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (!table1.isVisible()) {
                    table1.setVisible(true);
                    table1.getTableHeader().setVisible(true);
                    maLabel.setVisible(true);
                }
                if (futureMode) {
                    return;
                }
                String etfFilePath = FileUtils.getConfig("etfFilePath");
                String rqPoolFilePath = FileUtils.getConfig("rqPoolFilePath");
                if (poolCheckBox.isSelected()) {
                    if (filePath.equals(new File(etfFilePath).getAbsolutePath())) {
                        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(rqPoolFilePath), "UTF-8"))) {
                            JSONObject jsonObject = new JSONObject();
                            String line = null;
                            while (StringUtils.isNotBlank(line = bufferedReader.readLine())) {
                                String[] arr = line.split(",");
                                jsonObject.put(arr[0], arr[2]);
                            }
                            for (Integer idx = 0; idx < shareEntityList.size(); idx++) {
                                ShareEntity shareEntity = shareEntityList.get(idx);
                                String key = shareEntity.getSinaCode();
                                if (jsonObject.containsKey(key)) {
                                    defaultTableModel.setValueAt(jsonObject.getString(key), idx, ColumnEnum.TABLE_GJ.getCode());
                                }
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    } else {
                        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(etfFilePath), "UTF-8"))) {
                            StringBuilder stringBuilder = new StringBuilder();
                            String line = null;
                            while (StringUtils.isNotBlank(line = bufferedReader.readLine())) {
                                stringBuilder.append(line);
                            }
                            String text = stringBuilder.toString();
                            for (Integer idx = 0; idx < shareEntityList.size(); idx++) {
                                ShareEntity shareEntity = shareEntityList.get(idx);
                                if (text.contains(shareEntity.getSinaCode())) {
                                    defaultTableModel.setValueAt("已有", idx, ColumnEnum.TABLE_GJ.getCode());
                                }
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                } else {
                    for (Integer idx = 0; idx < shareEntityList.size(); idx++) {
                        defaultTableModel.setValueAt(null, idx, ColumnEnum.TABLE_GJ.getCode());
                    }
                }
            }
        });
        greenFilterCheckBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (!table1.isVisible()) {
                    table1.setVisible(true);
                    table1.getTableHeader().setVisible(true);
                    maLabel.setVisible(true);
                }
                if (greenFilterCheckBox.isSelected()) {
                    redFilterCheckBox.setSelected(false);
                }
                refreshRowFilter();
            }
        });
        redFilterCheckBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (!table1.isVisible()) {
                    table1.setVisible(true);
                    table1.getTableHeader().setVisible(true);
                    maLabel.setVisible(true);
                }
                if (redFilterCheckBox.isSelected()) {
                    greenFilterCheckBox.setSelected(false);
                }
                refreshRowFilter();
            }
        });
    }

    //表右键菜单初始化
    private void initPopupMenuList() {
        if (null == jPopupMenu) {
            jPopupMenu = new JPopupMenu();
        } else {
            jPopupMenu.removeAll();
        }
        JMenuItem fs = new JMenuItem("分时");
        fs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayFSPicture();
            }
        });
        jPopupMenu.add(fs);
        JMenuItem rk = new JMenuItem("日k");
        rk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayRKPicture();
            }
        });
        jPopupMenu.add(rk);
        if (!futureMode) {
            JMenuItem gk = new JMenuItem("概况");
            gk.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    displayGK();
                }
            });
            jPopupMenu.add(gk);
        }
        if (futureMode) {
            String text = null;
            if ("fi".equals(futureCodeType)) {
                text = "主力";
            } else {
                text = "加权";
            }
            JMenuItem futureOne = new JMenuItem(text);
            String finalText = text;
            futureOne.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Integer idx = table1.getSelectedRow();
                    if (idx >= 0) {
                        ShareEntity shareEntity = shareEntityList.get((Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1));
                        if ("加权".equals(finalText)) {
                            displayFiRZYPicture(shareEntity.getEastCode());
                        } else {
                            displayMainRZYPicture(shareEntity.getEastCode());
                        }
                    }
                }
            });
            jPopupMenu.add(futureOne);
        } else {
            JMenuItem rzk = new JMenuItem("日周k");
            rzk.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    displayRZKPicture();
                }
            });
            jPopupMenu.add(rzk);
        }
        JMenuItem rzyk = new JMenuItem("周月k");
        rzyk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayRZYKPicture(null);
            }
        });
        jPopupMenu.add(rzyk);
        if (futureMode) {
            String text = null;
            if ("sec".equals(futureCodeType)) {
                text = "主力";
            } else {
                text = "次主";
            }
            JMenuItem futureSec = new JMenuItem(text);
            String finalText = text;
            futureSec.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Integer idx = table1.getSelectedRow();
                    if (idx >= 0) {
                        ShareEntity shareEntity = shareEntityList.get((Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1));
                        if ("主力".equals(finalText)) {
                            displayMainRZYPicture(shareEntity.getEastCode());
                        } else {
                            displaySecRZYPicture(shareEntity.getEastCode());
                        }
                    }
                }
            });
            jPopupMenu.add(futureSec);
        }
        table1.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3) {
                    jPopupMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
    }

    private void saveToFile() {
        if (shareEntityList.stream().anyMatch(item -> StringUtils.isBlank(item.getShareName()))) {
            return;
        }
        File file = new File(filePath);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false), "UTF-8"))) {
            bufferedWriter.write(StringUtils.join(shareEntityList.stream().map(item -> {
                if (StringUtils.isNotBlank(item.getRemark())) {
                    return String.format("%s,%s,%s", futureMode ? item.getEastCode() : item.getSinaCode().toLowerCase(), item.getShareName(), item.getRemark());
                } else {
                    return String.format("%s,%s", futureMode ? item.getEastCode() : item.getSinaCode().toLowerCase(), item.getShareName());
                }
            }).collect(Collectors.toList()), "\n"));
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private void updatePool() {
        Integer idx = table1.getSelectedRow();
        if (idx >= 0) {
            ShareEntity shareEntity = shareEntityList.get((Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1));
            String bj = (String) table1.getValueAt(idx, ColumnEnum.TABLE_GJ.getCode());
            String filePath = FileUtils.getConfig("etfFilePath");
            //插入末尾
            if (StringUtils.isBlank(bj)) {
                try (BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath, true), "UTF-8"))) {
                    bufferedWriter.write(String.format("\n%s,%s", shareEntity.getSinaCode(),
                            StringUtils.isBlank(shareEntity.getShareName()) ? "无" : shareEntity.getShareName()));
                    table1.setValueAt("已有", idx, ColumnEnum.TABLE_GJ.getCode());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
            //删除
            else {
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
                    String line = null;
                    List<String> list = new ArrayList<>();
                    while (StringUtils.isNotBlank(line = bufferedReader.readLine())) {
                        if (!line.contains(shareEntity.getSinaCode())) {
                            list.add(line);
                        }
                    }
                    bufferedReader.close();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath, false), "UTF-8"));
                    bufferedWriter.write(StringUtils.join(list.stream().map(item -> String.format("%s", item)).collect(Collectors.toList()), "\n"));
                    bufferedWriter.close();
                    table1.setValueAt(null, idx, ColumnEnum.TABLE_GJ.getCode());
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    private byte[] getSharePicture(String url, String eastCode, String period, Integer idx) throws Exception {
        HttpGet httpGet = null;
        try {
            httpGet = new HttpGet(url);
            if (url.contains(ipAndPort)) {
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(5000).build());
            } else {
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).build());
            }
            byte[] bytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
            if (!drawSelfCheckBox.isSelected()) {
                ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                if (addTextWatermark(inputStream, outputStream, eastCode, period, (String) defaultTableModel.getValueAt(Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1, ColumnEnum.TABLE_BZ.getCode()))) {
                    bytes = outputStream.toByteArray();
                }
                inputStream.close();
                outputStream.close();
            }
            if (Objects.isNull(bytes) || bytes.length < 1024) {
                throw new Exception();
            }
            return bytes;
        } catch (Exception e) {
            throw e;
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
    }

    //显示分时图
    private void displayFSPicture() {
        Integer idx = table1.getSelectedRow();
        if (idx >= 0) {
            HttpGet httpGet = null;
            try {
                ShareEntity shareEntity = shareEntityList.get(Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1);
                httpGet = new HttpGet(String.format("http://%s/real?code=%s&name=%s&today=%s&market=%s",
                        ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(), shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2)));
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                File file = File.createTempFile("ShareFollow分时", ".png");
                file.deleteOnExit();
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity()));
                fileOutputStream.close();
                Desktop.getDesktop().open(file);
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                if (Objects.nonNull(httpGet)) {
                    httpGet.releaseConnection();
                }
            }
        }
    }

    //显示日线图
    private void displayRKPicture() {
        Integer idx = table1.getSelectedRow();
        if (idx >= 0) {
            HttpGet httpGet = null;
            try {
                ShareEntity shareEntity = shareEntityList.get(Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1);
                if (drawSelfCheckBox.isSelected()) {
                    httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=day&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                            shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2)));
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                } else {
                    httpGet = new HttpGet(String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL", shareEntity.getEastCode()));
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                }
                byte[] bytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                if (!drawSelfCheckBox.isSelected()) {
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    String g10 = (String) table1.getValueAt(idx, ColumnEnum.TABLE_G10.getCode());
                    String g20 = (String) table1.getValueAt(idx, ColumnEnum.TABLE_G20.getCode());
                    String priceText = (String) table1.getValueAt(idx, ColumnEnum.TABLE_JG.getCode());
                    String changeText = (String) table1.getValueAt(idx, ColumnEnum.TABLE_ZDF.getCode());
                    if (addTextWatermark(inputStream, outputStream, shareEntity.getEastCode(), "day", (String) defaultTableModel.getValueAt(Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1, ColumnEnum.TABLE_BZ.getCode()))) {
                        bytes = outputStream.toByteArray();
                    }
                    inputStream.close();
                    outputStream.close();
                }
                File file = File.createTempFile("ShareFollow日线", ".png");
                file.deleteOnExit();
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(bytes);
                fileOutputStream.close();
                Desktop.getDesktop().open(file);
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                if (Objects.nonNull(httpGet)) {
                    httpGet.releaseConnection();
                }
            }
        }
    }

    //显示概况
    private void displayGK() {
        Integer idx = table1.getSelectedRow();
        if (idx >= 0) {
            try {
                ShareEntity shareEntity = shareEntityList.get((Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1));
                String sinaCode = shareEntity.getSinaCode();
                if ("sh000001".equals(sinaCode)) {
                    return;
                }
                Summary summary = new Summary(panel1);
                if (futureMode) {
                    summary.query(shareEntity.getEastCode());
                } else {
                    summary.query(shareEntity.getShareCode());
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    //显示日周线图
    private void displayRZKPicture() {
        Integer idx = table1.getSelectedRow();
        if (idx >= 0) {
            HttpGet httpGet = null;
            try {
                ShareEntity shareEntity = shareEntityList.get((Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1));
                byte[] dayBytes = null;
                byte[] weekBytes = null;
                if (drawSelfCheckBox.isSelected()) {
                    httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=day&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                            shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2)));
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                    dayBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                    httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=week&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                            shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2)));
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                    weekBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                } else {
                    String dayUrl = String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=D", shareEntity.getEastCode());
                    httpGet = new HttpGet(dayUrl);
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                    dayBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                    ByteArrayInputStream dayInputStream = new ByteArrayInputStream(dayBytes);
                    ByteArrayOutputStream dayOutputStream = new ByteArrayOutputStream();
                    if (addTextWatermark(dayInputStream, dayOutputStream, shareEntity.getEastCode(), "day", (String) defaultTableModel.getValueAt(Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1, ColumnEnum.TABLE_BZ.getCode()))) {
                        dayBytes = dayOutputStream.toByteArray();
                    }
                    dayInputStream.close();
                    dayOutputStream.close();
                    String weekUrl = String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=W", shareEntity.getEastCode());
                    httpGet = new HttpGet(weekUrl);
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                    weekBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                    ByteArrayInputStream weekInputStream = new ByteArrayInputStream(weekBytes);
                    ByteArrayOutputStream weekOutputStream = new ByteArrayOutputStream();
                    if (addTextWatermark(weekInputStream, weekOutputStream, shareEntity.getEastCode(), "week", null)) {
                        weekBytes = weekOutputStream.toByteArray();
                    }
                    weekInputStream.close();
                    weekOutputStream.close();
                }
                File file = File.createTempFile("ShareFollow日周线", ".png");
                file.deleteOnExit();
                FollowUtils.mergeImage(Arrays.asList(new ByteArrayInputStream(dayBytes), new ByteArrayInputStream(weekBytes)), file, false);
                Desktop.getDesktop().open(file);
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                if (Objects.nonNull(httpGet)) {
                    httpGet.releaseConnection();
                }
            }
        }
    }

    private void displayMainRZYPicture(String eastCode) {
        HttpGet httpGet = null;
        try {
            if (futureMode) {
                httpGet = new HttpGet(String.format("http://%s/future_basis?code=%s", ipAndPort, eastCode));
            } else {
                httpGet = new HttpGet(String.format("http://%s/etf_basis?code=%s", ipAndPort, eastCode.substring(2)));
            }
            JSONObject jsonObject = JSONObject.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity()));
            String mainCode = jsonObject.getString("mainCode");
            String name = jsonObject.getString("name");
            ShareEntity shareEntity = new ShareEntity();
            shareEntity.setShareCode(mainCode);
            shareEntity.setSinaCode(mainCode);
            shareEntity.setEastCode(mainCode);
            shareEntity.setShareName(name + "主力");
            displayRZYKPicture(shareEntity);
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
    }

    private void displaySecRZYPicture(String eastCode) {
        HttpGet httpGet = null;
        try {
            httpGet = new HttpGet(String.format("http://%s/future_basis?code=%s", ipAndPort, eastCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(5000).build());
            JSONObject jsonObject = JSONObject.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity()));
            String mainCode = jsonObject.getString("secCode");
            String name = jsonObject.getString("name");
            ShareEntity shareEntity = new ShareEntity();
            shareEntity.setShareCode(mainCode);
            shareEntity.setSinaCode(mainCode);
            shareEntity.setEastCode(mainCode);
            shareEntity.setShareName(name + "次主");
            displayRZYKPicture(shareEntity);
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
    }

    private void displayFiRZYPicture(String eastCode) {
        HttpGet httpGet = null;
        try {
            httpGet = new HttpGet(String.format("http://%s/future_basis?code=%s", ipAndPort, eastCode));
            JSONObject jsonObject = JSONObject.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity()));
            String fiCode = jsonObject.getString("fiCode");
            String name = jsonObject.getString("name");
            ShareEntity shareEntity = new ShareEntity();
            shareEntity.setShareCode(fiCode);
            shareEntity.setSinaCode(fiCode);
            shareEntity.setEastCode(fiCode);
            shareEntity.setShareName(name + "加权");
            displayRZYKPicture(shareEntity);
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
    }

    private void displayRefRZYPicture(String eastCode) {
        HttpGet httpGet = null;
        try {
            httpGet = new HttpGet(String.format("http://%s/future_basis?code=%s", ipAndPort, eastCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(5000).build());
            JSONObject jsonObject = JSONObject.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity()));
            String refCode = jsonObject.getString("refCode");
            if (StringUtils.isBlank(refCode)) {
                return;
            }
            String name = jsonObject.getString("name");
            ShareEntity shareEntity = new ShareEntity();
            shareEntity.setShareCode(refCode);
            shareEntity.setSinaCode(refCode);
            shareEntity.setEastCode(refCode);
            shareEntity.setShareName(name + "外盘");
            displayRZYKPicture(shareEntity);
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
    }

    //显示日周月线图
    private void displayRZYKPicture(ShareEntity shareEntity) {
        Integer idx = table1.getSelectedRow();
        if (idx >= 0) {
            HttpGet httpGet = null;
            try {
                if (null == shareEntity) {
                    shareEntity = shareEntityList.get((Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1));
                }
                byte[] dayBytes = null;
                byte[] weekBytes = null;
                byte[] monthBytes = null;
                if (drawSelfCheckBox.isSelected()) {
                    httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=day&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                            shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2)));
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                    dayBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                    httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=week&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                            shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2)));
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                    weekBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                    httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=month&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                            shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), futureMode ? "ft" : shareEntity.getSinaCode().substring(0, 2)));
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                    monthBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                } else {
                    String dayUrl = String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=D", shareEntity.getEastCode());
                    httpGet = new HttpGet(dayUrl);
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                    dayBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                    ByteArrayInputStream dayInputStream = new ByteArrayInputStream(dayBytes);
                    ByteArrayOutputStream dayOutputStream = new ByteArrayOutputStream();
                    if (addTextWatermark(dayInputStream, dayOutputStream, shareEntity.getEastCode(), "day", (String) defaultTableModel.getValueAt(Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1, ColumnEnum.TABLE_BZ.getCode()))) {
                        dayBytes = dayOutputStream.toByteArray();
                    }
                    dayInputStream.close();
                    dayOutputStream.close();
                    String weekUrl = String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=W", shareEntity.getEastCode());
                    httpGet = new HttpGet(weekUrl);
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                    weekBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                    ByteArrayInputStream weekInputStream = new ByteArrayInputStream(weekBytes);
                    ByteArrayOutputStream weekOutputStream = new ByteArrayOutputStream();
                    if (addTextWatermark(weekInputStream, weekOutputStream, shareEntity.getEastCode(), "week", null)) {
                        weekBytes = weekOutputStream.toByteArray();
                    }
                    weekInputStream.close();
                    weekOutputStream.close();
                    String monthUrl = String.format("http://webquoteklinepic.eastmoney.com/GetPic.aspx?nid=%s&unitWidth=-6&ef=&formula=MACD&imageType=KXL&type=M", shareEntity.getEastCode());
                    httpGet = new HttpGet(monthUrl);
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                    monthBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                    ByteArrayInputStream monthInputStream = new ByteArrayInputStream(monthBytes);
                    ByteArrayOutputStream monthOutputStream = new ByteArrayOutputStream();
                    if (addTextWatermark(monthInputStream, monthOutputStream, shareEntity.getEastCode(), "month", null)) {
                        monthBytes = monthOutputStream.toByteArray();
                    }
                    monthInputStream.close();
                    monthOutputStream.close();
                }
                File file = File.createTempFile("ShareFollow日周月线" + (futureMode ? shareEntity.getShareName() : ""), ".png");
                file.deleteOnExit();
                FollowUtils.mergeImage(Arrays.asList(new ByteArrayInputStream(dayBytes), new ByteArrayInputStream(weekBytes), new ByteArrayInputStream(monthBytes)), file, false);
                Desktop.getDesktop().open(file);
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                if (Objects.nonNull(httpGet)) {
                    httpGet.releaseConnection();
                }
            }
        }
    }

    //显示行业日周月线图
    private void displayMarketKPicture(String market) {
        if (futureMode) {
            return;
        }
        Integer idx = table1.getSelectedRow();
        if (idx >= 0) {
            HttpGet httpGet = null;
            try {
                ShareEntity shareEntity = shareEntityList.get((Integer.valueOf(table1.getValueAt(idx, ColumnEnum.TABLE_NO.getCode()).toString()) - 1));
                byte[] dayBytes = null;
                byte[] weekBytes = null;
                byte[] monthBytes = null;
                httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=day&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                        shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), market));
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                dayBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=week&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                        shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), market));
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                weekBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                httpGet = new HttpGet(String.format("http://%s/kline?code=%s&name=%s&today=%s&period=month&mock=0&market=%s", ipAndPort, futureMode ? shareEntity.getEastCode() : shareEntity.getShareCode(),
                        shareEntity.getShareName(), DATE_FORMATTER_Y_M_D_SPLIT.format(today), market));
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(5000).build());
                monthBytes = EntityUtils.toByteArray(closeableHttpClient.execute(httpGet).getEntity());
                File file = File.createTempFile("Market日周月线", ".png");
                file.deleteOnExit();
                FollowUtils.mergeImage(Arrays.asList(new ByteArrayInputStream(dayBytes), new ByteArrayInputStream(weekBytes), new ByteArrayInputStream(monthBytes)), file, false);
                Desktop.getDesktop().open(file);
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                if (Objects.nonNull(httpGet)) {
                    httpGet.releaseConnection();
                }
            }
        }
    }

    private Boolean addTextWatermark(ByteArrayInputStream inputStream, ByteArrayOutputStream outputStream, String eastCode, String period, String tag) {
        HttpGet httpGet = null;
        try {
            List<String> markList = new ArrayList<>();
            httpGet = new HttpGet(URI.create(String.format("http://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f51,f53,f56,f57&klt=101&fqt=1&secid=%s&beg=0&end=20500000", eastCode)));
            JSONArray jsonArray = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONObject("data").getJSONArray("klines");
            Integer jsonSize = jsonArray.size();
            Double realPrice = Double.valueOf(jsonArray.getString(jsonSize - 1).split(",")[1]);
            Calendar calendar = Calendar.getInstance();
            Map<Integer, Pair<Integer, Double>> map = new HashMap<>();
            Integer type = null;
            if ("day".equalsIgnoreCase(period)) {
                type = Calendar.DAY_OF_YEAR;
            } else if ("week".equalsIgnoreCase(period)) {
                type = Calendar.WEEK_OF_YEAR;
            } else if ("month".equalsIgnoreCase(period)) {
                type = Calendar.MONTH;
            }
            for (Integer i = jsonSize - 1, count = 1; i >= 0 && count <= 20; i--) {
                String[] array = jsonArray.getString(i).split(",");
                Date date = DateUtils.parseDate(array[0], YYYY_MM_DD_SPLIT);
                calendar.setTime(date);
                Integer key = Integer.valueOf(String.format("%04d%04d", calendar.get(Calendar.YEAR), calendar.get(type)));
                Pair<Integer, Double> pair = map.get(key);
                if (Objects.isNull(pair)) {
                    Double price = Double.valueOf(array[1]);
                    map.put(key, Pair.of(key, price));
                    count++;
                }
            }
            List<Pair<Integer, Double>> list = map.values().stream().sorted(Comparator.comparing(Pair::getLeft)).collect(Collectors.toList());
            Integer size = list.size();
            if (size >= 10) {
                Double ma10 = 0.0;
                for (Integer i = size - 10; i < size; i++) {
                    ma10 += list.get(i).getRight();
                }
                ma10 = ma10 / 10;
                markList.add(String.format("g10：%.2f%%", (realPrice - ma10) / ma10 * 100));
            }
            if (size >= 20) {
                Double ma20 = 0.0;
                for (Integer i = size - 20; i < size; i++) {
                    ma20 += list.get(i).getRight();
                }
                ma20 = ma20 / 20;
                markList.add(String.format("g20：%.2f%%", (realPrice - ma20) / ma20 * 100));
            }
            Double yesClose = list.get(size - 2).getRight();
            BufferedImage sourceImage = ImageIO.read(inputStream);
            Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();
            //设置文字透明度
            AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f);
            g2d.setComposite(alphaChannel);
            //设置文字字体
            g2d.setFont(new Font("宋体", Font.PLAIN, 13));
            //设置文字颜色
            Color textColor = Color.BLACK;
            g2d.setColor(textColor);
            int centerX = (int) (sourceImage.getWidth() * 0.4);
            int centerY = (int) (sourceImage.getHeight() * 0.12);
            //绘制文字
            if (CollectionUtils.isNotEmpty(markList)) {
                String text = StringUtils.join(markList, "，");
                g2d.drawString(text, centerX, centerY);
            }
            String priceText = String.format("%.3f", realPrice);
            String changeText = String.format("%.2f%%", (realPrice - yesClose) / yesClose * 100);
            centerX = (int) (sourceImage.getWidth() * 0.45);
            centerY = (int) (sourceImage.getHeight() * 0.09);
            g2d.drawString(String.format("%s，%s", priceText, changeText), centerX, centerY);
            if (futureMode && "day".equals(period)) {
                httpGet = new HttpGet(String.format("http://%s/future_basis?code=%s", ipAndPort, eastCode));
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                JSONObject jsonObject = JSONObject.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity()));
                Double unit = jsonObject.getDouble("unit");
                Double ratio = jsonObject.getDouble("ratio");
                centerX = (int) (sourceImage.getWidth() * 0.23);
                centerY = (int) (sourceImage.getHeight() * 0.53);
                String last = jsonArray.getString(jsonSize - 1);
                Double price = Double.valueOf(last.split(",")[1]);
                Double amount = Double.valueOf(last.split(",")[3]);
                g2d.drawString(String.format("成交额：%.2f亿，杠杆：%.2f倍，每手金额：%.2f万元", amount / 1e8, 1 / ratio, price * unit * ratio / 1e4), centerX, centerY);
            } else if (!futureMode && "day".equals(period)) {
                centerX = (int) (sourceImage.getWidth() * 0.45);
                centerY = (int) (sourceImage.getHeight() * 0.53);
                String last = jsonArray.getString(jsonSize - 1);
                Double amount = Double.valueOf(last.split(",")[3]);
                g2d.drawString(String.format("成交额：%.2f亿", amount / 1e8), centerX, centerY);
            }
            ImageIO.write(sourceImage, "png", outputStream);
            g2d.dispose();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
    }

    //告警
    private void warn(String title, String content) {
        if (Boolean.TRUE.equals(warnEnable)) {
            warnEnable = Boolean.FALSE;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    //通知提示
                    trayIcon.displayMessage(title, content, TrayIcon.MessageType.INFO);
                    if (futureMode) {
                        HttpPost httpPost = null;
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("text", content);
                            httpPost = new HttpPost(String.format("http://%s/speak", ipAndPort));
                            StringEntity stringEntity = new StringEntity(jsonObject.toString(), "UTF-8");
                            stringEntity.setContentType("application/json");
                            httpPost.setEntity(stringEntity);
                            httpPost.setConfig(RequestConfig.custom().setConnectTimeout(50).build());
                            EntityUtils.toString(closeableHttpClient.execute(httpPost).getEntity());
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (Objects.nonNull(httpPost)) {
                                httpPost.releaseConnection();
                            }
                        }
                    }
                    String sleepSeconds = FileUtils.getConfig("sleepSeconds");
                    if (StringUtils.isBlank(sleepSeconds)) {
                        sleepSeconds = "30";
                    }
                    //屏蔽30秒内的其他告警信息
                    FollowUtils.mySleep(Integer.valueOf(sleepSeconds) * 1000);
                    warnEnable = Boolean.TRUE;
                }
            }).start();
        }
    }

    private void start() {
        try {
            String startDateString = FileUtils.getConfig("thsSentence");
            Matcher m = FollowUtils.Y_M_D_CN_PATTERN.matcher(startDateString);
            if (m.find()) {
                startDate = LocalDate.parse(m.group(), DATE_FORMATTER_Y_M_D_CN);
            }
            String text = FileUtils.getConfig("min1Threshold");
            if (StringUtils.isNotBlank(text)) {
                min1Threshold = Double.valueOf(text);
            }
            nine30 = today.atTime(9, 30, 0);
            eleven30 = today.atTime(11, 30, 0);
            thirteen = today.atTime(13, 0, 0);
            fifteen = today.atTime(15, 0, 0);
            realFuture = scheduledExecutorService.scheduleAtFixedRate(new RealJob(), 0, 1, TimeUnit.SECONDS);
            viewFuture = scheduledExecutorService.scheduleAtFixedRate(new ViewJob(), 1, 1, TimeUnit.SECONDS);
            maFuture = scheduledExecutorService.scheduleAtFixedRate(new MaJob(), 1, Math.max(Double.valueOf(shareEntityList.size() * 0.5).intValue(), 30), TimeUnit.SECONDS);
            volFuture = scheduledExecutorService.scheduleAtFixedRate(new VolJob(), 1, Math.max(Double.valueOf(shareEntityList.size() * 0.5).intValue(), 30), TimeUnit.SECONDS);
            realMaFuture = scheduledExecutorService.scheduleAtFixedRate(new RealMaJob(), 1, 10, TimeUnit.SECONDS);
            //初始化区间涨跌幅起始点
            scheduledExecutorService.submit(new Runnable() {
                @Override
                public void run() {
                    //long t1 = System.currentTimeMillis();
                    //计算区间涨跌幅
                    HttpGet httpGet = null;
                    Integer size = shareEntityList.size();
                    for (Integer idx = 0; idx < size; idx++) {
                        ShareEntity shareEntity = shareEntityList.get(idx);
                        try {
                            httpGet = new HttpGet(String.format("https://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f51,f53,f56&klt=101&fqt=1&secid=%s&beg=0&end=20500000", shareEntity.getEastCode()));
                            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).build());
                            String entity = EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity());
                            JSONArray lineRK = JSON.parseObject(entity).getJSONObject("data").getJSONArray("klines");
                            if (futureMode) {
                                shareEntity.setYesClose(Double.valueOf(lineRK.getString(lineRK.size() - 2).split(",")[1]));
                                shareEntity.setYesVol(Double.valueOf(lineRK.getString(lineRK.size() - 2).split(",")[2]));
                            } else {
                                shareEntity.setYesClose(Double.valueOf(lineRK.getString(lineRK.size() - 2).split(",")[1]));
                                shareEntity.setYesVol(Double.valueOf(lineRK.getString(lineRK.size() - 2).split(",")[2]) * 100);
                            }
                            shareEntity.setStartPoint(Double.valueOf(lineRK.getString(0).split(",")[1]));
                            for (Integer i = lineRK.size() - 2; i >= 0; i--) {
                                String[] arr = lineRK.getString(i).split(",");
                                LocalDate localDate = LocalDate.parse(arr[0], DATE_FORMATTER_Y_M_D_SPLIT);
                                if (localDate.compareTo(startDate) < 0) {
                                    shareEntity.setStartPoint(Double.valueOf(arr[1]));
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (Objects.nonNull(httpGet)) {
                                httpGet.releaseConnection();
                            }
                        }
                    }
                    //System.out.println("init qjzdf cost:" + (System.currentTimeMillis() - t1));
                    //初始化macd标记
                    long t2 = System.currentTimeMillis();
                    for (Integer idx = 0; idx < size; idx++) {
                        ShareEntity shareEntity = shareEntityList.get(idx);
                        try {
                            httpGet = new HttpGet(String.format("http://%s/macd?code=%s", ipAndPort, shareEntity.getShareCode()));
                            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).build());
                            String macd = EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity());
                            shareEntity.setMacd(StringUtils.isNotBlank(macd) ? macd : null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (Objects.nonNull(httpGet)) {
                                httpGet.releaseConnection();
                            }
                        }
                    }
                    //System.out.println("init macd cost:" + (System.currentTimeMillis() - t2));
                }
            });
            startButton.setText("Stop");
            startButton.setEnabled(true);
            saveButton.setEnabled(true);
            clearButton.setEnabled(true);
            openButton.setEnabled(false);
            try {
                tray.add(trayIcon);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private void stop() {
        try {
            realFuture.cancel(true);
            viewFuture.cancel(true);
            maFuture.cancel(true);
            volFuture.cancel(true);
            realMaFuture.cancel(true);
            startButton.setText("Start");
            startButton.setEnabled(true);
            openButton.setEnabled(true);
            tray.remove(trayIcon);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void initWindow() {
        openButton.setEnabled(true);
        startButton.setEnabled(false);
        saveButton.setEnabled(false);
        clearButton.setEnabled(false);
        //数据模型
        defaultTableModel = new DefaultTableModel();
        defaultTableModel.setColumnIdentifiers(Arrays.asList(ColumnEnum.values()).stream().map(item -> item.getDesc()).collect(Collectors.toList()).toArray());
        //清空显示
        defaultTableModel.setRowCount(0);
        DefaultTableCellRenderer defaultTableCellRenderer = new DefaultTableCellRenderer();
        defaultTableCellRenderer.setHorizontalAlignment(JLabel.CENTER);
        table1.setDefaultRenderer(Object.class, defaultTableCellRenderer);
        table1.setRowSelectionAllowed(true);
        table1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table1.setModel(defaultTableModel);
        table1.setEnabled(true);
        //table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ////调整字体
        //table1.getTableHeader().setFont(new Font(null, Font.PLAIN, 8));
        //table1.setFont(new Font(null, Font.PLAIN, 8));
        //设置字体颜色渲染
        for (Integer i = 0; i < ColumnEnum.values().length; i++) {
            table1.getColumnModel().getColumn(i).setCellRenderer(getCellRenderer());
        }
        //设置字体最大宽度
        table1.getColumnModel().getColumn(ColumnEnum.TABLE_NO.getCode()).setMinWidth(40);
        table1.getColumnModel().getColumn(ColumnEnum.TABLE_NO.getCode()).setMaxWidth(40);
        table1.getColumnModel().getColumn(ColumnEnum.TABLE_MACD.getCode()).setMinWidth(60);
        table1.getColumnModel().getColumn(ColumnEnum.TABLE_MACD.getCode()).setMaxWidth(60);
        table1.getColumnModel().getColumn(ColumnEnum.TABLE_JLR.getCode()).setMinWidth(60);
        table1.getColumnModel().getColumn(ColumnEnum.TABLE_JLR.getCode()).setMaxWidth(60);
        table1.getColumnModel().getColumn(ColumnEnum.TABLE_BZ.getCode()).setMinWidth(100);
        tray = SystemTray.getSystemTray();
        trayIcon = new TrayIcon(new ImageIcon(this.getClass().getResource("icon.png")).getImage(), "ShareFollow");
        trayIcon.setImageAutoSize(true);
    }

    //获取字体颜色渲染
    private DefaultTableCellRenderer getCellRenderer() {
        return new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Double temp = 0.0;
                try {
                    //根据第3列的数值渲染当前行颜色
                    String rs = (String) table1.getValueAt(row, ColumnEnum.TABLE_ZDF.getCode());
                    if (StringUtils.isNotBlank(rs)) {
                        temp = Double.valueOf(rs.endsWith("%") ? rs.substring(0, rs.length() - 1) : rs);
                    }
                } catch (Exception e) {
                    setForeground(Color.BLACK);
                    setHorizontalAlignment(JLabel.CENTER);
                }
                if (temp > 0) {
                    setForeground(Color.BLACK);
                    setHorizontalAlignment(JLabel.CENTER);
                } else {
                    setForeground(Color.GRAY);
                    setHorizontalAlignment(JLabel.CENTER);
                }
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        };
    }

    //预估今日全天成交量
    class VolJob implements Runnable {
        @Override
        public void run() {
            //long t1 = System.currentTimeMillis();
            Integer timeIndex = 0;
            LocalDateTime now = LocalDateTime.now();
            if (now.isBefore(nine30)) {
                timeIndex = 239;
            } else if (now.isBefore(eleven30)) {
                timeIndex = ((int) Duration.between(nine30, now).getSeconds() + 60) / 60 - 1;
            } else if (now.isBefore(thirteen)) {
                timeIndex = 119;
            } else if (now.isBefore(fifteen)) {
                timeIndex = ((int) Duration.between(nine30, now).getSeconds() + 60) / 60 - 91;
            } else {
                timeIndex = 239;
            }
            Integer size = shareEntityList.size();
            for (Integer idx = 0; idx < size; idx++) {
                ShareEntity shareEntity = shareEntityList.get(idx);
                if (null != shareEntity.getTodayRealVol()) {
                    if (futureMode) {
                        Double preVal = shareEntity.getTodayVol();
                        Double newVal = shareEntity.getTodayRealVol();
                        if (null != preVal) {
                            if (newVal > preVal) {
                                shareEntity.setOnTrade(true);
                            } else {
                                shareEntity.setOnTrade(false);
                            }
                        }
                        shareEntity.setTodayVol(newVal);
                        continue;
                    }
                    double[] yesRealVol = yesRealVolCache.get(shareEntity.getEastCode());
                    //不存在则初始化
                    if (Objects.isNull(yesRealVol)) {
                        HttpGet httpGet = null;
                        try {
                            String shareCode = shareEntity.getShareCode();
                            String market = null;
                            if (shareCode.charAt(0) == '0' || shareCode.charAt(0) == '3') {
                                market = "SZ";
                            } else if (shareCode.charAt(0) == '1') {
                                market = "SZETF";
                            } else if (shareCode.charAt(0) == '5') {
                                market = "SHETF";
                            } else if (shareCode.charAt(0) == '6') {
                                market = "SH";
                            }
                            //上证指数特殊处理
                            if ("sh000001".equals(shareEntity.getSinaCode())) {
                                market = "SH";
                            }
                            //查询昨日分时数据，将MIN1换成DAY就是日K线
                            httpGet = new HttpGet(URI.create(String.format("https://hq.techgp.cn/rjhy-gmg-quote/api/1/stock/quote/v1/historyv1?endtime=%d&inst=%s&limit=-240&market=%s&servicetype=KLINEB&starttime=0&resptype=JSON&period=MIN1",
                                    yesterday.atTime(23, 59, 59).toInstant(ZoneOffset.of("+8")).getEpochSecond(), shareCode, market)));
                            JSONArray jsonArray = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONArray("KlineData");
                            Integer jsonSize = jsonArray.size();
                            long sum = 0L;
                            yesRealVol = new double[240];
                            for (Integer i = 0; i < jsonSize; i++) {
                                sum = sum + jsonArray.getJSONObject(i).getLong("Volume");
                                yesRealVol[i] = sum;
                            }
                            yesRealVolCache.put(shareEntity.getEastCode(), yesRealVol);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (Objects.nonNull(httpGet)) {
                                httpGet.releaseConnection();
                            }
                        }
                    }
                    shareEntity.setYesRealVol(yesRealVol[timeIndex]);
                    Double vdb = shareEntity.getTodayRealVol() * 1.0 / shareEntity.getYesRealVol();
                    //上证指数要特殊处理，乘以100
                    if ("sh000001".equals(shareEntity.getSinaCode())) {
                        vdb = vdb * 100;
                    }
                    if (null != shareEntity.getYesVol()) {
                        shareEntity.setTodayVol(shareEntity.getYesVol() * vdb);
                    }
                }
            }
            //System.out.println("vol:" + (System.currentTimeMillis() - t1));
        }
    }

    class ViewJob implements Runnable {
        @Override
        public void run() {
            //long t1 = System.currentTimeMillis();
            Integer size = shareEntityList.size();
            StringBuilder stringBuilder = new StringBuilder();
            Boolean hasWarn = false;
            Boolean poolCheckBoxSelected = poolCheckBox.isSelected();
            String cur = (String) comboBox1.getSelectedItem();
            for (Integer idx = 0; idx < size; idx++) {
                ShareEntity shareEntity = shareEntityList.get(idx);
                Double realPrice = shareEntity.getRealPrice();
                //序号
                defaultTableModel.setValueAt(String.format("%03d", idx + 1), idx, ColumnEnum.TABLE_NO.getCode());
                //名称
                if (realPrice <= 0.0) {
                    defaultTableModel.setValueAt("(停牌)" + shareEntity.getShareName() + shareEntity.getShareCode(), idx, ColumnEnum.TABLE_MC.getCode());
                    continue;
                } else {
                    if (futureMode && poolCheckBoxSelected && Boolean.FALSE.equals(shareEntity.getOnTrade())) {
                        defaultTableModel.setValueAt("(收盘)" + shareEntity.getShareName() + shareEntity.getShareCode(), idx, ColumnEnum.TABLE_MC.getCode());
                    } else {
                        defaultTableModel.setValueAt(shareEntity.getShareName() + shareEntity.getShareCode(), idx, ColumnEnum.TABLE_MC.getCode());
                    }
                }
                //价格
                defaultTableModel.setValueAt(String.format("%.3f", realPrice), idx, ColumnEnum.TABLE_JG.getCode());
                //涨跌幅%
                defaultTableModel.setValueAt(String.format("%.2f%%", shareEntity.getChange()), idx, ColumnEnum.TABLE_ZDF.getCode());
                MaEntity maEntity = shareEntity.getMaEntity();
                if (null != maEntity.getV20()) {
                    Double v20 = maEntity.getV20();
                    String shrink = null;
                    if (v20 <= v20Threshold.getLeft()) {
                        shrink = "*";
                    } else if (v20 >= v20Threshold.getRight()) {
                        shrink = "#";
                    } else {
                        shrink = " ";
                    }
                    defaultTableModel.setValueAt(String.format("%.2f%s", v20, shrink), idx, ColumnEnum.TABLE_V20.getCode());
                }
                String dayFlag = "";
                if (null != maEntity.getMa20()) {
                    if (1 == maEntity.getMa20Direction()) {
                        if (realPrice >= maEntity.getMa20()) {
                            dayFlag = "d";
                        }
                    } else if (-1 == maEntity.getMa20Direction()) {
                        if (realPrice < maEntity.getMa20()) {
                            dayFlag = "1";
                        }
                    }
                }
                String weekFlag = "";
                if (null != maEntity.getWma20()) {
                    if (1 == maEntity.getWma20Direction()) {
                        if (realPrice >= maEntity.getWma20()) {
                            weekFlag = "w";
                        }
                    } else if (-1 == maEntity.getWma20Direction()) {
                        if (realPrice < maEntity.getWma20()) {
                            weekFlag = "3";
                        }
                    }
                }
                String monthFlag = "";
                if (null != maEntity.getMma20()) {
                    if (1 == maEntity.getMma20Direction()) {
                        if (realPrice >= maEntity.getMma20()) {
                            monthFlag = "m";
                        }
                    } else if (-1 == maEntity.getMma20Direction()) {
                        if (realPrice < maEntity.getMma20()) {
                            monthFlag = "5";
                        }
                    }
                }
                //10日线乖离率
                if (null != maEntity.getMa10()) {
                    defaultTableModel.setValueAt(String.format("%.2f%%", (shareEntity.getRealPrice() - maEntity.getMa10()) / maEntity.getMa10() * 100), idx, ColumnEnum.TABLE_G10.getCode());
                }
                String gn = "";
                //20日线乖离率
                if (null != maEntity.getMa20()) {
                    Double bias = (shareEntity.getRealPrice() - maEntity.getMa20()) / maEntity.getMa20() * 100;
                    if (cur.contains("周线") && !cur.contains("日")) {
                        if (null != maEntity.getWma20()) {
                            Double weekBias = (shareEntity.getRealPrice() - maEntity.getWma20()) / maEntity.getWma20() * 100;
                            defaultTableModel.setValueAt(String.format("%.2f%%", weekBias), idx, ColumnEnum.TABLE_G20.getCode());
                        } else {
                            defaultTableModel.setValueAt(null, idx, ColumnEnum.TABLE_G20.getCode());
                        }
                    } else if (cur.contains("月线") && !cur.contains("周")) {
                        if (null != maEntity.getMma20()) {
                            Double monthBias = (shareEntity.getRealPrice() - maEntity.getMma20()) / maEntity.getMma20() * 100;
                            defaultTableModel.setValueAt(String.format("%.2f%%", monthBias), idx, ColumnEnum.TABLE_G20.getCode());
                        } else {
                            defaultTableModel.setValueAt(null, idx, ColumnEnum.TABLE_G20.getCode());
                        }
                    } else {
                        defaultTableModel.setValueAt(String.format("%.2f%%", bias), idx, ColumnEnum.TABLE_G20.getCode());
                    }
                    String code = shareEntity.getShareCode();
                    //etf用板块的阈值
                    if (code.startsWith("1") || code.startsWith("5")) {
                        if (bias >= g1Market.getLeft() && bias <= g1Market.getRight()) {
                            gn = "g1";
                        } else if (bias >= g2Market.getLeft() && bias <= g2Market.getRight()) {
                            gn = "g2";
                        }
                    } else {
                        if (bias >= g1Share.getLeft() && bias <= g1Share.getRight()) {
                            gn = "g1";
                        } else if (bias >= g2Share.getLeft() && bias <= g2Share.getRight()) {
                            gn = "g2";
                        } else if (bias >= g3Share.getLeft() && bias <= g3Share.getRight()) {
                            gn = "g3";
                        }
                    }
                }
                //分钟乖离率
                if (futureMode && "分钟模式".equals(cur)) {
                    if (null != maEntity.getMin1Ma10()) {
                        Double bias10 = (shareEntity.getRealPrice() - maEntity.getMin1Ma10()) / maEntity.getMin1Ma10() * 100;
                        defaultTableModel.setValueAt(String.format("%.2f%%", bias10), idx, ColumnEnum.TABLE_G10.getCode());
                    } else {
                        defaultTableModel.setValueAt(null, idx, ColumnEnum.TABLE_G10.getCode());
                    }
                    if (null != maEntity.getMin1Ma20() && null != min1Threshold) {
                        Double bias20 = (shareEntity.getRealPrice() - maEntity.getMin1Ma20()) / maEntity.getMin1Ma20() * 100;
                        defaultTableModel.setValueAt(String.format("%.2f%%", bias20), idx, ColumnEnum.TABLE_G20.getCode());
                        if (bias20 >= min1Threshold) {
                            String bz = (String) defaultTableModel.getValueAt(idx, ColumnEnum.TABLE_BZ.getCode());
                            if (StringUtils.isBlank(bz) || (StringUtils.isNotBlank(bz) && !bz.contains("忽略"))) {
                                hasWarn = true;
                                stringBuilder.append(String.format("%s空空超涨%.2f,", shareEntity.getShareName(), bias20));
                            }
                        } else if (bias20 <= -min1Threshold) {
                            String bz = (String) defaultTableModel.getValueAt(idx, ColumnEnum.TABLE_BZ.getCode());
                            if (StringUtils.isBlank(bz) || (StringUtils.isNotBlank(bz) && !bz.contains("忽略"))) {
                                hasWarn = true;
                                stringBuilder.append(String.format("%s多多超跌%.2f,", shareEntity.getShareName(), bias20));
                            }
                        }
                    } else {
                        defaultTableModel.setValueAt(null, idx, ColumnEnum.TABLE_G20.getCode());
                    }
                }
                defaultTableModel.setValueAt(String.format("%s%s%s%s", dayFlag, weekFlag, monthFlag, gn), idx, ColumnEnum.TABLE_BJ.getCode());
                defaultTableModel.setValueAt(shareEntity.getMacd(), idx, ColumnEnum.TABLE_MACD.getCode());
                if (null != shareEntity.getJlrAmount()) {
                    defaultTableModel.setValueAt(String.format("%.2f", shareEntity.getJlrAmount() / 1e8), idx, ColumnEnum.TABLE_JLR.getCode());
                }
                //区间涨跌幅
                if (null != shareEntity.getQjzdf()) {
                    defaultTableModel.setValueAt(String.format("%.2f%%", shareEntity.getQjzdf()), idx, ColumnEnum.TABLE_QJZDF.getCode());
                }
                String SYZ = (String) defaultTableModel.getValueAt(idx, ColumnEnum.TABLE_SYZ.getCode());
                String XYZ = (String) defaultTableModel.getValueAt(idx, ColumnEnum.TABLE_XYZ.getCode());
                if (null != realPrice && StringUtils.isNotEmpty(SYZ)) {
                    if (SYZ.endsWith("%")) {
                        if (null != shareEntity.getChange() && shareEntity.getChange() >= Double.valueOf(SYZ.substring(0, SYZ.length() - 1))) {
                            defaultTableModel.setValueAt("↑" + SYZ, idx, ColumnEnum.TABLE_GJ.getCode());
                            hasWarn = true;
                            stringBuilder.append(idx + 1).append(shareEntity.getShareName()).append("↑" + SYZ + ",");
                        }
                    } else {
                        if (shareEntity.getRealPrice() >= Double.valueOf(SYZ)) {
                            defaultTableModel.setValueAt("↑" + SYZ, idx, ColumnEnum.TABLE_GJ.getCode());
                            hasWarn = true;
                            stringBuilder.append(idx + 1).append(shareEntity.getShareName()).append("↑" + SYZ + ",");
                        }
                    }
                }
                if (null != realPrice && StringUtils.isNotEmpty(XYZ)) {
                    if (XYZ.endsWith("%")) {
                        if (null != shareEntity.getChange() && shareEntity.getChange() <= Double.valueOf(XYZ.substring(0, XYZ.length() - 1))) {
                            defaultTableModel.setValueAt("↓" + XYZ, idx, ColumnEnum.TABLE_GJ.getCode());
                            hasWarn = true;
                            stringBuilder.append(idx + 1).append(shareEntity.getShareName()).append("↓" + XYZ + ",");
                        }
                    } else {
                        if (shareEntity.getRealPrice() <= Double.valueOf(XYZ)) {
                            defaultTableModel.setValueAt("↓" + XYZ, idx, ColumnEnum.TABLE_GJ.getCode());
                            hasWarn = true;
                            stringBuilder.append(idx + 1).append(shareEntity.getShareName()).append("↓" + XYZ + ",");
                        }
                    }
                }
            }
            if (true == hasWarn) {
                warn("Windows Update", stringBuilder.toString());
            }
            //打印本地时间
            timeLabel.setText(TIME_FORMATTER_H_M_S.format(LocalTime.now()));
            //System.out.println("view:" + (System.currentTimeMillis() - t1));
        }
    }

    class MaJob implements Runnable {
        @Override
        public void run() {
            //long t1 = System.currentTimeMillis();
            HttpGet httpGet = null;
            Integer size = shareEntityList.size();
            for (Integer idx = 0; idx < size; idx++) {
                try {
                    ShareEntity shareEntity = shareEntityList.get(idx);
                    Double realPrice = shareEntity.getRealPrice();
                    double[] dayCloseArray = dayCloseCache.get(shareEntity.getEastCode());
                    double[] dayVolArray = dayVolCache.get(shareEntity.getEastCode());
                    double[] weekCloseArray = weekCloseCache.get(shareEntity.getEastCode());
                    double[] monthCloseArray = monthCloseCache.get(shareEntity.getEastCode());
                    //不存在则初始化
                    if (Objects.isNull(dayCloseArray) || Objects.isNull(dayVolArray) || Objects.isNull(weekCloseArray) || Objects.isNull(monthCloseArray)) {
                        //填充日线数据
                        httpGet = new HttpGet(URI.create(String.format("http://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f51,f53,f56&klt=101&fqt=1&secid=%s&beg=0&end=20500000", shareEntity.getEastCode())));
                        JSONArray jsonArray = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONObject("data").getJSONArray("klines");
                        Integer jsonSize = jsonArray.size();
                        if (null == realPrice) {
                            realPrice = Double.valueOf(jsonArray.getString(jsonSize - 1).split(",")[1]);
                        }
                        dayCloseArray = new double[21];
                        dayVolArray = new double[21];
                        for (Integer i = jsonSize - 1, cursor = 20; i >= 0 && cursor >= 0; i--, cursor--) {
                            String[] arr = jsonArray.getString(i).split(",");
                            Double price = Double.valueOf(arr[1]);
                            Double volume = Double.valueOf(arr[2]);
                            dayCloseArray[cursor] = price;
                            if (futureMode) {
                                dayVolArray[cursor] = volume;
                            } else {
                                //个股先暂时用实时成交量填充
                                dayVolArray[cursor] = volume * 100;
                            }
                        }
                        //填充周线数据
                        httpGet = new HttpGet(URI.create(String.format("http://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f51,f53,f56&klt=102&fqt=1&secid=%s&beg=0&end=20500000", shareEntity.getEastCode())));
                        jsonArray = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONObject("data").getJSONArray("klines");
                        jsonSize = jsonArray.size();
                        weekCloseArray = new double[21];
                        for (Integer i = jsonSize - 1, cursor = 20; i >= 0 && cursor >= 0; i--, cursor--) {
                            String[] arr = jsonArray.getString(i).split(",");
                            Double price = Double.valueOf(arr[1]);
                            weekCloseArray[cursor] = price;
                        }
                        //填充月线数据
                        httpGet = new HttpGet(URI.create(String.format("http://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f51,f53,f56&klt=103&fqt=1&secid=%s&beg=0&end=20500000", shareEntity.getEastCode())));
                        jsonArray = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONObject("data").getJSONArray("klines");
                        jsonSize = jsonArray.size();
                        monthCloseArray = new double[21];
                        for (Integer i = jsonSize - 1, cursor = 20; i >= 0 && cursor >= 0; i--, cursor--) {
                            String[] arr = jsonArray.getString(i).split(",");
                            Double price = Double.valueOf(arr[1]);
                            monthCloseArray[cursor] = price;
                        }
                    }
                    //更新实时数据
                    dayCloseArray[20] = realPrice;
                    if (null != shareEntity.getTodayVol()) {
                        dayVolArray[20] = shareEntity.getTodayVol();
                    }
                    weekCloseArray[20] = realPrice;
                    monthCloseArray[20] = realPrice;
                    MaEntity maEntity = shareEntity.getMaEntity();
                    if (dayCloseArray[21 - 5] > 0) {
                        Double priceSum = 0.0;
                        for (Integer i = 21 - 5; i < 21; i++) {
                            priceSum += dayCloseArray[i];
                        }
                        maEntity.setMa5(priceSum / 5);
                    }
                    if (dayCloseArray[21 - 10] > 0) {
                        Double priceSum = 0.0;
                        for (Integer i = 21 - 10; i < 21; i++) {
                            priceSum += dayCloseArray[i];
                        }
                        maEntity.setMa10(priceSum / 10);
                    }
                    //需要判断方向，需要多一个点的数据
                    if (dayCloseArray[0] > 0) {
                        Double priceSum = 0.0;
                        Double volSum = 0.0;
                        for (Integer i = 1; i < 21; i++) {
                            priceSum += dayCloseArray[i];
                            volSum += dayVolArray[i];
                        }
                        maEntity.setMa20(priceSum / 20);
                        dayMa20.put(shareEntity.getShareCode(), (realPrice - maEntity.getMa20()) / maEntity.getMa20() * 100);
                        maEntity.setV20(dayVolArray[20] / volSum * 20);
                        if (realPrice > dayCloseArray[0]) {
                            maEntity.setMa20Direction(1);
                        } else if (realPrice < dayCloseArray[0]) {
                            maEntity.setMa20Direction(-1);
                        } else {
                            maEntity.setMa20Direction(0);
                        }
                    } else {
                        maEntity.setMa20Direction(0);
                    }
                    if (weekCloseArray[0] > 0) {
                        Double priceSum = 0.0;
                        for (Integer i = 1; i < 21; i++) {
                            priceSum += weekCloseArray[i];
                        }
                        maEntity.setWma20(priceSum / 20);
                        weekMa20.put(shareEntity.getShareCode(), (realPrice - maEntity.getWma20()) / maEntity.getWma20() * 100);
                        if (realPrice > weekCloseArray[0]) {
                            maEntity.setWma20Direction(1);
                        } else if (realPrice < weekCloseArray[0]) {
                            maEntity.setWma20Direction(-1);
                        } else {
                            maEntity.setWma20Direction(0);
                        }
                    } else {
                        maEntity.setWma20Direction(0);
                    }
                    if (monthCloseArray[0] > 0) {
                        Double priceSum = 0.0;
                        for (Integer i = 1; i < 21; i++) {
                            priceSum += monthCloseArray[i];
                        }
                        maEntity.setMma20(priceSum / 20);
                        monthMa20.put(shareEntity.getShareCode(), (realPrice - maEntity.getMma20()) / maEntity.getMma20() * 100);
                        if (realPrice > monthCloseArray[0]) {
                            maEntity.setMma20Direction(1);
                        } else if (realPrice < monthCloseArray[0]) {
                            maEntity.setMma20Direction(-1);
                        } else {
                            maEntity.setMma20Direction(0);
                        }
                    } else {
                        maEntity.setMma20Direction(0);
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                } finally {
                    if (Objects.nonNull(httpGet)) {
                        httpGet.releaseConnection();
                    }
                }
            }
            if (!futureMode) {
                try {
                    httpGet = new HttpGet(URI.create("https://push2.eastmoney.com/api/qt/clist/get?fid=f62&po=0&pz=10000&pn=1&np=1&fltt=2&invt=2&fs=m:0+t:6+f:!2,m:0+t:13+f:!2,m:0+t:80+f:!2,m:1+t:2+f:!2,m:1+t:23+f:!2,m:0+t:7+f:!2,m:1+t:3+f:!2&fields=f3,f6,f12,f62"));
                    httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).build());
                    JSONArray jsonArray = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONObject("data").getJSONArray("diff");
                    Integer jsonSize = jsonArray.size();
                    Integer upCnt = 0;
                    Integer downCnt = 0;
                    Double aAmt = 0.0;
                    for (Integer index = 0; index < jsonSize; index++) {
                        JSONObject jso = jsonArray.getJSONObject(index);
                        String code = jso.getString("f12");
                        Double percent = NumberUtils.toDouble(jso.getString("f3"));
                        Double amount = NumberUtils.toDouble(jso.getString("f6"));
                        if (percent >= 0.0) {
                            upCnt++;
                        } else {
                            downCnt++;
                        }
                        aAmt += amount;
                        ShareEntity shareEntity = shareEntityList.stream().filter(item -> item.getShareCode().equals(code)).findFirst().orElse(null);
                        if (null != shareEntity && !"sh000001".equals(shareEntity.getSinaCode())) {
                            shareEntity.setJlrAmount(NumberUtils.toDouble(jso.getString("f62")));
                        }
                    }
                    upCount = upCnt;
                    downCount = downCnt;
                    aAmount = aAmt;
                } catch (Exception e1) {
                    e1.printStackTrace();
                } finally {
                    if (Objects.nonNull(httpGet)) {
                        httpGet.releaseConnection();
                    }
                }
            }
            //System.out.println("ma:" + (System.currentTimeMillis() - t1));
        }
    }

    class RealMaJob implements Runnable {
        @Override
        public void run() {
            if (!futureMode) {
                return;
            }
            //long t1 = System.currentTimeMillis();
            HttpGet httpGet = null;
            Integer size = shareEntityList.size();
            for (Integer idx = 0; idx < size; idx++) {
                try {
                    ShareEntity shareEntity = shareEntityList.get(idx);
                    MaEntity maEntity = shareEntity.getMaEntity();
                    //不存在则初始化
                    if (null != maEntity) {
                        httpGet = new HttpGet(URI.create(String.format("http://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f53&klt=1&fqt=1&secid=%s&beg=0&end=20500000", shareEntity.getEastCode())));
                        JSONArray jsonArray = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONObject("data").getJSONArray("klines");
                        Integer jsonSize = jsonArray.size();
                        double price10Sum = 0.0;
                        double price20Sum = 0.0;
                        Integer count = 1;
                        for (Integer i = jsonSize - 1; i >= 0 && count <= 20; i--) {
                            String[] arr = jsonArray.getString(i).split(",");
                            Double price = Double.valueOf(arr[0]);
                            if (count <= 10) {
                                price10Sum += price;
                            }
                            if (count <= 20) {
                                price20Sum += price;
                            }
                            count++;
                        }
                        if (count >= 10) {
                            maEntity.setMin1Ma10(price10Sum / 10);
                        } else {
                            maEntity.setMin1Ma10(0.0);
                        }
                        if (count >= 20) {
                            maEntity.setMin1Ma20(price20Sum / 20);
                        } else {
                            maEntity.setMin1Ma20(0.0);
                        }
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                } finally {
                    if (Objects.nonNull(httpGet)) {
                        httpGet.releaseConnection();
                    }
                }
            }
            //System.out.println("min1Ma:" + (System.currentTimeMillis() - t1));
        }
    }

    class RealJob implements Runnable {
        //耗时固定，大多为50ms
        @Override
        public void run() {
            //long t1 = System.currentTimeMillis();
            try {
                CloseableHttpResponse realResponse = closeableHttpClient.execute(realHttpGet);
                String rs = EntityUtils.toString(realResponse.getEntity());
                if (futureMode) {
                    JSONArray jsonArray = JSON.parseObject(rs).getJSONObject("data").getJSONArray("diff");
                    Integer size = jsonArray.size();
                    for (Integer idx = 0; idx < size; idx++) {
                        JSONObject obj = jsonArray.getJSONObject(idx);
                        ShareEntity shareEntity = shareEntityList.get(idx);
                        //价格倍数
                        Double ratio = Math.pow(10, Double.valueOf(obj.getDouble("f1")));
                        shareEntity.setOpenPrice(Double.valueOf(obj.getDouble("f17")) / ratio);
                        shareEntity.setHighPrice(Double.valueOf(obj.getDouble("f15")) / ratio);
                        shareEntity.setLowPrice(Double.valueOf(obj.getDouble("f16")) / ratio);
                        shareEntity.setRealPrice(Double.valueOf(obj.getDouble("f2")) / ratio);
                        shareEntity.setTodayRealVol(Double.valueOf(obj.getDouble("f5")));
                        shareEntity.setTodayRealAmt(Double.valueOf(obj.getDouble("f6")));
                        shareEntity.setJlrAmount(shareEntity.getTodayRealAmt());
                        Double yesClose = shareEntity.getYesClose();
                        if (null == yesClose) {
                            shareEntity.setChange(0.0);
                        } else {
                            shareEntity.setChange((shareEntity.getRealPrice() - yesClose) / yesClose * 100);
                        }
                        Double startPoint = shareEntity.getStartPoint();
                        if (Objects.nonNull(startPoint)) {
                            shareEntity.setQjzdf((shareEntity.getRealPrice() - startPoint) / startPoint * 100);
                        }
                    }
                } else {
                    List<String> results = Arrays.asList(rs.replaceAll("\n", "").split(";"));
                    Integer size = results.size();
                    //解析结果
                    for (Integer idx = 0; idx < size; idx++) {
                        ShareEntity shareEntity = shareEntityList.get(idx);
                        String[] line = results.get(idx).split("\"");
                        if (line.length < 2) {
                            shareEntity.setRealPrice(0.0);
                            continue;
                        }
                        String[] arr = line[1].split(",");
                        if (arr.length < 2) {
                            shareEntity.setRealPrice(0.0);
                            continue;
                        }
                        shareEntity.setShareName(arr[0].replaceAll(" ", ""));
                        shareEntity.setOpenPrice(Double.valueOf(arr[1]));
                        Double yesClose = Double.valueOf(arr[2]);
                        Double realPrice = Double.valueOf(arr[3]);
                        shareEntity.setRealPrice(realPrice);
                        shareEntity.setYesClose(yesClose);
                        shareEntity.setHighPrice(Double.valueOf(arr[4]));
                        shareEntity.setLowPrice(Double.valueOf(arr[5]));
                        shareEntity.setTodayRealVol(Double.valueOf(arr[8]));
                        shareEntity.setTodayRealAmt(Double.valueOf(arr[9]));
                        if (shareEntity.getShareCode().startsWith("1") || shareEntity.getShareCode().startsWith("5")) {
                            shareEntity.setJlrAmount(shareEntity.getTodayRealAmt());
                        }
                        shareEntity.setChange((realPrice - yesClose) / yesClose * 100);
                        Double startPoint = shareEntity.getStartPoint();
                        if (Objects.nonNull(startPoint)) {
                            shareEntity.setQjzdf((shareEntity.getRealPrice() - startPoint) / startPoint * 100);
                        }
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            } finally {
                //经常访问保持长链接暂时不释放
                //realHttpGet.releaseConnection();
            }
            //System.out.println("real:" + (System.currentTimeMillis() - t1));
        }
    }

    class DeviationJob implements Runnable {
        @Override
        public void run() {
            //long t1 = System.currentTimeMillis();
            Integer size = shareEntityList.size();
            StringBuilder stringBuilder = new StringBuilder();
            Boolean hasWarn = false;
            for (Integer idx = 0; idx < size; idx++) {
                String text = (String) defaultTableModel.getValueAt(idx, ColumnEnum.TABLE_BZ.getCode());
                if (StringUtils.isNotBlank(text)) {
                    ShareEntity shareEntity = shareEntityList.get(idx);
                    HttpGet httpGet = null;
                    if (text.contains("买") || text.contains("卖")) {
                        try {
                            httpGet = new HttpGet(String.format("http://%s/deviation?code=%s&minutes=1", ipAndPort, shareEntity.getShareCode()));
                            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(3000).build());
                            String rs = EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity());
                            if (StringUtils.isNotBlank(rs) && ((text.contains("买") && rs.equals("W")) || (text.contains("卖") && rs.equals("M")))) {
                                String signal = rs.equals("W") ? "Buy" : "Sell";
                                defaultTableModel.setValueAt(signal, idx, ColumnEnum.TABLE_GJ.getCode());
                                stringBuilder.append(idx + 1).append(shareEntity.getShareName()).append(" ").append(signal);
                                hasWarn = true;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (Objects.nonNull(httpGet)) {
                                httpGet.releaseConnection();
                            }
                        }
                    }
                }
            }
            if (true == hasWarn) {
                warn("Windows Deviation", stringBuilder.toString());
            }
            //System.out.println("deviation:" + (System.currentTimeMillis() - t1));
        }
    }

    private Boolean isEastFutureCode(String code) {
        if (code.contains(".")) {
            return true;
        } else {
            return false;
        }
    }

    private void refreshRowFilter() {
        String key = (String) comboBox1.getSelectedItem();
        String value = FollowUtils.COMBOBOX_MAP.get(key);
        Boolean green = greenFilterCheckBox.isSelected();
        Boolean red = redFilterCheckBox.isSelected();
        TableRowSorter sorter = (TableRowSorter) table1.getRowSorter();
        if (StringUtils.isNotBlank(value)) {
            if (key.contains("回踩") || key.contains("反抽")) {
                List<RowFilter<Object, Object>> result = new ArrayList<>();
                result.add(RowFilter.regexFilter(value.split(",")[0], ColumnEnum.TABLE_BJ.getCode()));
                result.add(RowFilter.regexFilter(value.split(",")[1], ColumnEnum.TABLE_G20.getCode()));
                if (green) {
                    result.add(RowFilter.regexFilter("^-.+%$", ColumnEnum.TABLE_ZDF.getCode()));
                } else if (red) {
                    result.add(RowFilter.regexFilter("^\\d{1,}.\\d{1,}%$", ColumnEnum.TABLE_ZDF.getCode()));
                }
                sorter.setRowFilter(RowFilter.andFilter(result));
            } else {
                if (green) {
                    List<RowFilter<Object, Object>> result = new ArrayList<>();
                    result.add(RowFilter.regexFilter(value, ColumnEnum.TABLE_BJ.getCode()));
                    result.add(RowFilter.regexFilter("^-.+%$", ColumnEnum.TABLE_ZDF.getCode()));
                    sorter.setRowFilter(RowFilter.andFilter(result));
                } else if (red) {
                    List<RowFilter<Object, Object>> result = new ArrayList<>();
                    result.add(RowFilter.regexFilter(value, ColumnEnum.TABLE_BJ.getCode()));
                    result.add(RowFilter.regexFilter("^\\d{1,}.\\d{1,}%$", ColumnEnum.TABLE_ZDF.getCode()));
                    sorter.setRowFilter(RowFilter.andFilter(result));
                } else {
                    sorter.setRowFilter(RowFilter.regexFilter(value, ColumnEnum.TABLE_BJ.getCode()));
                }
            }
        } else {
            if (green) {
                sorter.setRowFilter(RowFilter.regexFilter("^-.+%$", ColumnEnum.TABLE_ZDF.getCode()));
            } else if (red) {
                sorter.setRowFilter(RowFilter.regexFilter("^\\d{1,}.\\d{1,}%$", ColumnEnum.TABLE_ZDF.getCode()));
            } else {
                sorter.setRowFilter(null);
            }
        }
    }
}
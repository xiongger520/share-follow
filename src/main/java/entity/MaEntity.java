package entity;

import java.io.Serializable;

//均线实体类
public class MaEntity implements Serializable {
    private static final long serialVersionUID = 2273358789758042473L;
    //5日均线
    private Double ma5;
    //10日均线
    private Double ma10;
    //20日均线
    private Double ma20;
    //20日均线方向
    private Integer ma20Direction;
    //20周均线
    private Double wma20;
    //20周均线方向
    private Integer wma20Direction;
    //20月均线
    private Double mma20;
    //20月均线方向
    private Integer mma20Direction;
    //10分钟均线
    private Double min1Ma10;
    //20分钟均线
    private Double min1Ma20;
    //今日全天成交量/20日平均成交量
    private Double v20;

    public MaEntity() {
    }

    public Double getMa5() {
        return ma5;
    }

    public void setMa5(Double ma5) {
        this.ma5 = ma5;
    }

    public Double getMa10() {
        return ma10;
    }

    public void setMa10(Double ma10) {
        this.ma10 = ma10;
    }

    public Double getMa20() {
        return ma20;
    }

    public void setMa20(Double ma20) {
        this.ma20 = ma20;
    }

    public Integer getMa20Direction() {
        return ma20Direction;
    }

    public void setMa20Direction(Integer ma20Direction) {
        this.ma20Direction = ma20Direction;
    }

    public Double getWma20() {
        return wma20;
    }

    public void setWma20(Double wma20) {
        this.wma20 = wma20;
    }

    public Integer getWma20Direction() {
        return wma20Direction;
    }

    public void setWma20Direction(Integer wma20Direction) {
        this.wma20Direction = wma20Direction;
    }

    public Double getMma20() {
        return mma20;
    }

    public void setMma20(Double mma20) {
        this.mma20 = mma20;
    }

    public Integer getMma20Direction() {
        return mma20Direction;
    }

    public void setMma20Direction(Integer mma20Direction) {
        this.mma20Direction = mma20Direction;
    }

    public Double getMin1Ma10() {
        return min1Ma10;
    }

    public void setMin1Ma10(Double min1Ma10) {
        this.min1Ma10 = min1Ma10;
    }

    public Double getMin1Ma20() {
        return min1Ma20;
    }

    public void setMin1Ma20(Double min1Ma20) {
        this.min1Ma20 = min1Ma20;
    }

    public Double getV20() {
        return v20;
    }

    public void setV20(Double v20) {
        this.v20 = v20;
    }
}

package entity;

import java.io.Serializable;
import java.util.Objects;

//股票实体类
public class ShareEntity implements Serializable {
    private static final long serialVersionUID = -539917818354526602L;
    //股票名称
    private String shareName;
    //股票代码
    private String shareCode;
    //东财code
    private String eastCode;
    //新浪code
    private String sinaCode;
    //实时价格
    private Double realPrice;
    //涨跌幅
    private Double change;
    //昨日实时成交量
    private Double yesRealVol;
    //今日实时成交量
    private Double todayRealVol;
    //今日实时成交额（用于计算分时均价线/结算价）
    private Double todayRealAmt;
    //昨日全天成交量
    private Double yesVol;
    //今日全天成交量（估计值）
    private Double todayVol;
    //开盘价
    private Double openPrice;
    //昨日收盘价
    private Double yesClose;
    //最高价
    private Double highPrice;
    //最低价
    private Double lowPrice;
    //均线
    private MaEntity maEntity;
    //macd
    private String macd;
    //净流入/成交金额
    private Double jlrAmount;
    //区间涨跌幅起始点位
    private Double startPoint;
    //区间涨跌幅
    private Double qjzdf;
    //上阈值
    private String SYZ;
    //上阈值
    private String XYZ;
    //备注
    private String remark;
    //交易中
    private Boolean onTrade;

    public ShareEntity() {
    }

    public String getShareName() {
        return shareName;
    }

    public void setShareName(String shareName) {
        this.shareName = shareName;
    }

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public String getEastCode() {
        return eastCode;
    }

    public void setEastCode(String eastCode) {
        this.eastCode = eastCode;
    }

    public String getSinaCode() {
        return sinaCode;
    }

    public void setSinaCode(String sinaCode) {
        this.sinaCode = sinaCode;
    }

    public Double getRealPrice() {
        return realPrice;
    }

    public void setRealPrice(Double realPrice) {
        this.realPrice = realPrice;
    }

    public Double getChange() {
        return change;
    }

    public void setChange(Double change) {
        this.change = change;
    }

    public Double getYesRealVol() {
        return yesRealVol;
    }

    public void setYesRealVol(Double yesRealVol) {
        this.yesRealVol = yesRealVol;
    }

    public Double getTodayRealVol() {
        return todayRealVol;
    }

    public void setTodayRealVol(Double todayRealVol) {
        this.todayRealVol = todayRealVol;
    }

    public Double getTodayRealAmt() {
        return todayRealAmt;
    }

    public void setTodayRealAmt(Double todayRealAmt) {
        this.todayRealAmt = todayRealAmt;
    }

    public Double getYesVol() {
        return yesVol;
    }

    public void setYesVol(Double yesVol) {
        this.yesVol = yesVol;
    }

    public Double getTodayVol() {
        return todayVol;
    }

    public void setTodayVol(Double todayVol) {
        this.todayVol = todayVol;
    }

    public Double getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(Double openPrice) {
        this.openPrice = openPrice;
    }

    public Double getYesClose() {
        return yesClose;
    }

    public void setYesClose(Double yesClose) {
        this.yesClose = yesClose;
    }

    public Double getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(Double highPrice) {
        this.highPrice = highPrice;
    }

    public Double getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(Double lowPrice) {
        this.lowPrice = lowPrice;
    }

    public MaEntity getMaEntity() {
        return maEntity;
    }

    public void setMaEntity(MaEntity maEntity) {
        this.maEntity = maEntity;
    }

    public String getMacd() {
        return macd;
    }

    public void setMacd(String macd) {
        this.macd = macd;
    }

    public Double getJlrAmount() {
        return jlrAmount;
    }

    public void setJlrAmount(Double jlrAmount) {
        this.jlrAmount = jlrAmount;
    }

    public Double getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(Double startPoint) {
        this.startPoint = startPoint;
    }

    public Double getQjzdf() {
        return qjzdf;
    }

    public void setQjzdf(Double qjzdf) {
        this.qjzdf = qjzdf;
    }

    public String getSYZ() {
        return SYZ;
    }

    public void setSYZ(String SYZ) {
        this.SYZ = SYZ;
    }

    public String getXYZ() {
        return XYZ;
    }

    public void setXYZ(String XYZ) {
        this.XYZ = XYZ;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getOnTrade() {
        return onTrade;
    }

    public void setOnTrade(Boolean onTrade) {
        this.onTrade = onTrade;
    }

    @Override
    public int hashCode() {
        return Objects.hash(shareCode);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ShareEntity) {
            ShareEntity shareEntity = (ShareEntity) obj;
            return shareEntity.shareCode.equals(shareCode) ? true : false;
        } else {
            return false;
        }
    }
}

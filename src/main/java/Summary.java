import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import common.FileUtils;
import common.FollowUtils;
import common.TimeConstant;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.URI;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;


public class Summary extends JFrame {
    private static String ipAndPort = null;
    private JPanel panel1;
    private JEditorPane jEditorPane;
    private JTextField textField1;
    private JButton refreshButton;
    private CloseableHttpClient httpClient = HttpClients.createDefault();

    public Summary(JPanel jPanel) {
        this.getRootPane().registerKeyboardAction(e -> {
            this.dispose();
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        setContentPane(panel1);
        setSize(500, 280);
        setLocationRelativeTo(jPanel);
        setTitle("Summary");
        String property = FileUtils.getConfig("ipAndPort");
        if (StringUtils.isNotBlank(property)) {
            ipAndPort = property;
        } else {
            ipAndPort = "127.0.0.1:1234";
        }
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = textField1.getText();
                if (StringUtils.isNotBlank(text)) {
                    jEditorPane.setText(null);
                    query(text);
                }
            }
        });
        textField1.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    refreshButton.doClick();
                }
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
    }

    public String queryFutureSummary(String eastCode) {
        HttpGet httpGet = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            httpGet = new HttpGet(String.format("http://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f51,f53,f57&klt=101&fqt=1&secid=%s&beg=0&end=20500000", eastCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).build());
            JSONArray jsonArray = JSON.parseObject(EntityUtils.toString(httpClient.execute(httpGet).getEntity())).getJSONObject("data").getJSONArray("klines");
            Integer jsonSize = jsonArray.size();
            String last = jsonArray.getString(jsonSize - 1);
            Double price = Double.valueOf(last.split(",")[1]);
            httpGet = new HttpGet(String.format("http://%s/future_basis?code=%s", ipAndPort, eastCode));
            JSONObject jsonObject = JSONObject.parseObject(EntityUtils.toString(httpClient.execute(httpGet).getEntity()));
            String code = jsonObject.getString("code");
            String name = jsonObject.getString("name");
            Double unit = jsonObject.getDouble("unit");
            Double ratio = jsonObject.getDouble("ratio");
            String ampl = jsonObject.getString("ampl");
            String fee = jsonObject.getString("fee");
            String tradeTime = jsonObject.getString("tradeTime");
            String mainCode = jsonObject.getString("mainCode").split("\\.")[1];
            String secCode = jsonObject.getString("secCode").split("\\.")[1];
            String otherCode = jsonObject.getString("otherCode");
            String exchCode = jsonObject.getString("exchCode");
            String exchName = jsonObject.getString("exchName");
            stringBuilder.append(String.format("1、品种：%s%s\n", name, eastCode));
            stringBuilder.append(String.format("2、主力合约代码：%s，次主力合约代码：%s\n", mainCode, secCode));
            stringBuilder.append(String.format("3、其他合约代码：%s\n", otherCode));
            stringBuilder.append(String.format("4、成交额：%.2f亿元\n", Double.valueOf(last.split(",")[2]) / 1e8));
            stringBuilder.append(String.format("5、交易单位：%s\n", unit.toString()));
            stringBuilder.append(String.format("6、公司保证金比例：%.2f%%，杠杆：%.2f倍\n", ratio * 100, 1 / ratio));
            stringBuilder.append(String.format("7、每手金额：%.2f万元\n", price * unit * ratio / 1e4));
            stringBuilder.append(String.format("8、交易所：%s\n", exchName));
            stringBuilder.append(String.format("9、交易费用：%s\n", fee));
            stringBuilder.append(String.format("10、交易时间：%s\n", tradeTime));
            stringBuilder.append(String.format("11、最大涨跌幅：%s\n", ampl));
            //查询最近一个交易日的日期
            httpGet.setURI(URI.create("http://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f51&klt=101&fqt=1&secid=1.000001&beg=0&end=20500000"));
            jsonArray = JSON.parseObject(EntityUtils.toString(httpClient.execute(httpGet).getEntity())).getJSONObject("data").getJSONArray("klines");
            String todayString = jsonArray.getString(jsonArray.size() - 1);
            Boolean latestFlag = true;
            if (LocalDateTime.now().isBefore(LocalDate.parse(todayString).atTime(16, 0, 0))) {
                todayString = jsonArray.getString(jsonArray.size() - 2);
                latestFlag = false;
            }
            String monthCode = null;
            Matcher m = FollowUtils.FUTURE_MONTH_PATTERN.matcher(mainCode);
            if (m.find()) {
                monthCode = m.group();
                if (monthCode.length() < 4) {
                    monthCode = "2" + monthCode;
                }
            }
            String futureCode = null;
            if ("225".equals(exchCode) || "142".equals(exchCode)) {
                futureCode = code + monthCode;
            } else {
                futureCode = code.toUpperCase() + monthCode;
            }
            httpGet.setURI(URI.create("https://datacenter-web.eastmoney.com/api/data/v1/get?reportName=RPT_FUTU_DAILYPOSITION&columns=ALL&filter=(SECURITY_CODE%3D%22{0}%22)(TRADE_DATE%3D%27{1}%27)(TYPE%3D%220%22)(NLPRANK%3C%3E9999)&sortTypes=1&sortColumns=NLPRANK&pageNumber=1&pageSize=10"
                    .replace("{0}", futureCode).replace("{1}", todayString)));
            jsonArray = JSON.parseObject(EntityUtils.toString(httpClient.execute(httpGet).getEntity())).getJSONObject("result").getJSONArray("data");
            jsonSize = jsonArray.size();
            stringBuilder.append(String.format("\n日期：%s（%s）", todayString, latestFlag ? "今日" : "昨日"));
            stringBuilder.append("\n---------------------------->净多头龙虎榜<----------------------------\n");
            for (Integer idx = 0; idx < jsonSize; idx++) {
                JSONObject item = jsonArray.getJSONObject(idx);
                String seatName = item.getString("MEMBER_NAME_ABBR");
                String longNum = item.getString("NET_LONG_POSITION");
                Integer chaneNum = item.getInteger("NLP_CHANGE");
                stringBuilder.append(String.format("%d、%s，%s，%+d\n", idx + 1, seatName, longNum, chaneNum));
            }
            httpGet = new HttpGet("https://datacenter-web.eastmoney.com/api/data/v1/get?reportName=RPT_FUTU_DAILYPOSITION&columns=ALL&filter=(SECURITY_CODE%3D%22{0}%22)(TRADE_DATE%3D%27{1}%27)(TYPE%3D%220%22)(NSPRANK%3C%3E9999)&sortTypes=1&sortColumns=NSPRANK&pageNumber=1&pageSize=10"
                    .replace("{0}", futureCode).replace("{1}", todayString));
            jsonArray = JSON.parseObject(EntityUtils.toString(httpClient.execute(httpGet).getEntity())).getJSONObject("result").getJSONArray("data");
            jsonSize = jsonArray.size();
            stringBuilder.append("\n---------------------------->净空头龙虎榜<----------------------------\n");
            for (Integer idx = 0; idx < jsonSize; idx++) {
                JSONObject item = jsonArray.getJSONObject(idx);
                String seatName = item.getString("MEMBER_NAME_ABBR");
                String longNum = item.getString("NET_SHORT_POSITION");
                Integer chaneNum = item.getInteger("NSP_CHANGE");
                stringBuilder.append(String.format("%d、%s，%s，%+d\n", idx + 1, seatName, longNum, chaneNum));
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        return stringBuilder.toString();
    }

    public static String querySummary(String shareCode, CloseableHttpClient closeableHttpClient, JEditorPane jEditorPane, String ipAndPort) {
        HttpGet httpGet = null;
        Document brief = null;
        String eastCode = null;
        if (shareCode.startsWith("6")) {
            eastCode = "SH" + shareCode;
        } else {
            eastCode = "SZ" + shareCode;
        }
        try {
            brief = Jsoup.connect(String.format("http://basic.10jqka.com.cn/%s/", shareCode)).get();
        } catch (IOException e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("network error!");
            }
            e1.printStackTrace();
        }
        StringBuilder stringBuilder = new StringBuilder();
        String shareName = brief.title().split("\\(")[0];
        stringBuilder.append(String.format("1、%s%s\n", shareName, shareCode));
        Element bright = brief.getElementsByClass("tip f14 fl core-view-text").first();
        if (null == bright) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("股票代码错误!");
            }
            return null;
        }
        stringBuilder.append(String.format("2、亮点：%s。\n", bright.text()));
        stringBuilder.append(String.format("3、业务：%s\n", brief.getElementsByAttributeValue("newtaid", "f10_zxdt_code-gsgy-r1c1-clickcontent-quota-zyyewu").first().text()));
        Element element = brief.getElementsByClass("f14  newconcept").first();
        String concepts = element.text().replace("， ", "、");
        stringBuilder.append(String.format("4、概念：%s\n", concepts.substring(0, concepts.length() - 5)));
        //公司全称和所在省份
        try {
            httpGet = new HttpGet(String.format("http://emweb.securities.eastmoney.com/PC_HSF10/CompanySurvey/PageAjax?code=%s", eastCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
            JSONObject jsonObject = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONArray("jbzl").getJSONObject(0);
            String fullName = jsonObject.getString("ORG_NAME");
            String province = jsonObject.getString("PROVINCE");
            stringBuilder.append(String.format("5、全称：%s-%s\n", province, fullName));
        } catch (Exception e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("network error!");
            }
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        //价格
        try {
            String sinaCode = null;
            if (shareCode.startsWith("6")) {
                sinaCode = "sh" + shareCode;
            } else {
                sinaCode = "sz" + shareCode;
            }
            httpGet = new HttpGet(String.format("http://hq.sinajs.cn/list=%s", sinaCode));
            httpGet.addHeader("Referer", "https://finance.sina.com.cn");
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(500).build());
            String rs = EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity());
            if (StringUtils.isNotBlank(rs)) {
                String[] arr = rs.split("\"")[1].split(",");
                Double realPrice = Double.valueOf(arr[3]);
                stringBuilder.append(String.format("6、价格：%.2f元\n", Double.valueOf(realPrice)));
            }
        } catch (Exception e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("network error!");
            }
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        String zsz = brief.getElementById("stockzsz").text();
        String zgb = brief.getElementById("stockzgb").val();
        String ltgb = brief.getElementsMatchingOwnText("流通A股：").first().nextElementSibling().text();
        //流通比例
        Double ltbl = Double.valueOf(ltgb.substring(0, ltgb.length() - 2)) / Double.valueOf(zgb);
        stringBuilder.append(String.format("7、总市值：%s，流通比例：%.0f%%\n", zsz, ltbl * 100));
        String pettm = null;
        try {
            httpGet = new HttpGet(String.format("http://basic.10jqka.com.cn/mapp/%s/a_companies_rank.json", shareCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
            JSONObject jsonObject = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity()));
            pettm = jsonObject.getJSONObject("data").getJSONArray("this_value").getJSONObject(0).getString("pe_ttm");
        } catch (IOException e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("network error!");
            }
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        if (StringUtils.isBlank(pettm)) {
            stringBuilder.append("8、TTM市盈率：无，");
        } else {
            if ("亏损".equals(pettm)) {
                stringBuilder.append("8、TTM市盈率：亏损，");
            } else {
                stringBuilder.append(String.format("8、TTM市盈率：%.0f倍，", Double.valueOf(pettm)));
            }
        }
        //动态市盈率
        String dtsyl = brief.getElementById("dtsyl").text();
        if (StringUtils.isBlank(dtsyl)) {
            stringBuilder.append("动态市盈率：无\n");
        } else {
            if ("亏损".equals(dtsyl)) {
                stringBuilder.append("动态市盈率：亏损\n");
            } else {
                stringBuilder.append(String.format("动态市盈率：%.0f倍\n", Double.valueOf(dtsyl)));
            }
        }
        //净利润
        String jlr = brief.getElementsMatchingOwnText("净利润：").first().nextElementSibling().text();
        if (StringUtils.isBlank(jlr)) {
            stringBuilder.append("9、净利润（前n季度）：无\n");
        } else {
            stringBuilder.append(String.format("9、净利润（前n季度）：%s\n", jlr));
        }
        //前3年净利润
        try {
            StringBuilder pastProfit = new StringBuilder();
            httpGet = new HttpGet(String.format("https://emweb.securities.eastmoney.com/PC_HSF10/NewFinanceAnalysis/ZYZBAjaxNew?type=1&code=%s", eastCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
            JSONArray jsonArray = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONArray("data");
            Integer size = jsonArray.size();
            for (Integer idx = 0; idx < 3 && idx < size; idx++) {
                JSONObject jsonObject = jsonArray.getJSONObject(idx);
                String year = LocalDateTime.parse(jsonObject.getString("REPORT_DATE"), TimeConstant.DATE_TIME_FORMATTER_Y_M_D_H_M_S).format(TimeConstant.DATE_FORMATTER_Y_CN);
                pastProfit.append(String.format("%s:%.2f亿，", year, jsonObject.getDouble("PARENTNETPROFIT") / 1e8));
            }
            if (pastProfit.length() > 0) {
                stringBuilder.append(String.format("10、前3年净利润：%s\n", pastProfit.deleteCharAt(pastProfit.length() - 1).toString()));
            } else {
                stringBuilder.append(String.format("10、前3年净利润：%s\n", "无"));
            }
        } catch (Exception e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("network error!");
            }
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        //毛利率和负债率
        try {
            httpGet = new HttpGet(String.format("http://emweb.securities.eastmoney.com/PC_HSF10/OperationsRequired/PageAjax?code=%s", eastCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
            JSONObject jsonObject = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONArray("zxzb").getJSONObject(0);
            Double xsmll = jsonObject.getDouble("XSMLL");
            Double zcfzl = jsonObject.getDouble("ZCFZL");
            stringBuilder.append(String.format("11、毛利率：%.2f%%，负债率：%.2f%%\n", xsmll, zcfzl));
        } catch (Exception e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("network error!");
            }
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        Document profit = null;
        try {
            profit = Jsoup.connect(String.format("http://basic.10jqka.com.cn/%s/worth.html", shareCode)).get();
        } catch (IOException e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("network error!");
            }
            e1.printStackTrace();
        }
        String[] results = profit.getElementsByClass("tip clearfix").first().text().replace(" ", "").split("，");
        if (results.length > 1) {
            stringBuilder.append(String.format("12、盈利预测：%s，%s\n", results[results.length - 2], results[results.length - 1]));
        } else {
            stringBuilder.append("12、盈利预测：无\n");
        }
        //经营分析（按照行业、产品、地区）
        try {
            httpGet = new HttpGet(String.format("http://emweb.securities.eastmoney.com/PC_HSF10/BusinessAnalysis/PageAjax?code=%s", eastCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
            JSONArray jsonArray = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONArray("zygcfx");
            String lastUpdateTime = jsonArray.getJSONObject(0).getString("REPORT_DATE");
            List<String> marketList = new ArrayList<>();
            List<String> productList = new ArrayList<>();
            List<String> areaList = new ArrayList<>();
            Integer size = jsonArray.size();
            for (Integer idx = 0; idx < size; idx++) {
                JSONObject jsonObject = jsonArray.getJSONObject(idx);
                if (jsonObject.getString("REPORT_DATE").equals(lastUpdateTime)) {
                    if ("1".equals(jsonObject.getString("MAINOP_TYPE"))) {
                        marketList.add(String.format("%s：%.2f%%", jsonObject.getString("ITEM_NAME"), Objects.nonNull(jsonObject.getDouble("MBI_RATIO")) ? jsonObject.getDouble("MBI_RATIO") * 100 : 0.0));
                    }
                    if ("2".equals(jsonObject.getString("MAINOP_TYPE"))) {
                        productList.add(String.format("%s：%.2f%%", jsonObject.getString("ITEM_NAME"), Objects.nonNull(jsonObject.getDouble("MBI_RATIO")) ? jsonObject.getDouble("MBI_RATIO") * 100 : 0.0));
                    }
                    if ("3".equals(jsonObject.getString("MAINOP_TYPE"))) {
                        areaList.add(String.format("%s：%.2f%%", jsonObject.getString("ITEM_NAME"), Objects.nonNull(jsonObject.getDouble("MBI_RATIO")) ? jsonObject.getDouble("MBI_RATIO") * 100 : 0.0));
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(marketList)) {
                stringBuilder.append(String.format("【行业占比】%s\n", StringUtils.join(marketList, "、")));
            }
            if (CollectionUtils.isNotEmpty(productList)) {
                stringBuilder.append(String.format("【产品占比】%s\n", StringUtils.join(productList, "、")));
            }
            if (CollectionUtils.isNotEmpty(areaList)) {
                stringBuilder.append(String.format("【地区占比】%s\n", StringUtils.join(areaList, "、")));
            }
        } catch (Exception e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("network error!");
            }
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        //两融余额
        try {
            httpGet = new HttpGet(MessageFormat.format("https://datacenter-web.eastmoney.com/api/data/v1/get?reportName=RPTA_WEB_RZRQ_GGMX&columns=ALL&sortColumns=DATE&sortTypes=-1&pageNumber=1&pageSize=1&filter=(scode=%22{0}%22)", shareCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(3000).build());
            JSONObject jsonObject = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONObject("result");
            if (null != jsonObject) {
                JSONObject item = jsonObject.getJSONArray("data").getJSONObject(0);
                if (null != item && StringUtils.isNotBlank(item.getString("RZYE"))) {
                    stringBuilder.append(String.format("13、融资余额：%.3f亿元，融券余额：%.3f亿元\n", item.getDouble("RZYE") / 1e8, item.getDouble("RQYE") / 1e8));
                } else {
                    stringBuilder.append("13、融资余额：非标的，融券余额：非标的\n");
                }
            } else {
                stringBuilder.append("13、融资余额：非标的，融券余额：非标的\n");
            }
        } catch (Exception e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("两融余额查询失败!");
            }
            e1.printStackTrace();
            return null;
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        stringBuilder.append("【龙虎榜席位】myTag\n");
        stringBuilder.append("\n---------------------------->新闻资讯<----------------------------\n");
        try {
            profit = Jsoup.connect(String.format("https://guba.eastmoney.com/list,%s,1,f.html", shareCode)).get();
            Elements elements = profit.select("a[data-postid]");
            if (CollectionUtils.isNotEmpty(elements)) {
                Integer size = elements.size();
                for (Integer idx = 0; idx < 5 && idx < size; idx++) {
                    stringBuilder.append(String.format("%d、%s\n", idx + 1, elements.get(idx).text()));
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        //去韭菜公社查询异动解析
        String extra = null;
        try {
            httpGet = new HttpGet(String.format("http://%s/jiucai?name=%s", ipAndPort, shareName));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(3000).build());
            String rs = EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity());
            if (StringUtils.isNotBlank(rs)) {
                extra = rs.substring(7);
            }
        } catch (Exception e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("韭菜公社查询失败!");
            }
            e1.printStackTrace();
            return null;
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        stringBuilder.append("\n---------------------------->异动解析<----------------------------\n");
        stringBuilder.append(extra);
        stringBuilder.append("\n");
        //去同花顺查询概念详情
        Document concept = null;
        try {
            concept = Jsoup.connect(String.format("http://basic.10jqka.com.cn/%s/concept.html", shareCode)).get();
        } catch (IOException e1) {
            if (Objects.nonNull(jEditorPane)) {
                jEditorPane.setText("network error!");
            }
            e1.printStackTrace();
        }
        Elements elements = concept.getElementsByClass("extend_content");
        if (Objects.nonNull(elements) && !elements.isEmpty()) {
            stringBuilder.append("\n---------------------------->概念详情<----------------------------\n");
            for (Element element1 : elements) {
                String conceptName = element1.previousElementSibling().child(1).text().replaceAll(" ", "");
                String conceptDesc = "无";
                if (element1.childrenSize() > 0 && element1.child(0).childrenSize() > 0) {
                    conceptDesc = element1.child(0).child(0).text().replaceAll(" ", "");
                }
                stringBuilder.append(String.format("【%s】%s\n", conceptName, conceptDesc));
            }
        }
        stringBuilder.append("\n---------------------------->龙虎榜<----------------------------\n");
        Set<String> tag = new HashSet<>();
        try {
            httpGet = new HttpGet("https://datacenter-web.eastmoney.com/api/data/v1/get?reportName=RPT_LHB_BOARDDATE&columns=SECURITY_CODE%2CTRADE_DATE%2CTR_DATE&filter=(SECURITY_CODE%3D%22{0}%22)&pageNumber=1&pageSize=1000&sortTypes=-1&sortColumns=TRADE_DATE".replace("{0}", shareCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
            String rs = EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity());
            JSONObject data = JSONObject.parseObject(rs);
            JSONArray jsonArray = data.getJSONObject("result").getJSONArray("data");
            if (CollectionUtils.isNotEmpty(jsonArray)) {
                //买方席位
                StringBuilder buyer = new StringBuilder();
                //卖方席位
                StringBuilder solder = new StringBuilder();
                String date = jsonArray.getJSONObject(0).getString("TRADE_DATE").split(" ")[0];
                httpGet = new HttpGet("https://datacenter-web.eastmoney.com/api/data/v1/get?reportName=RPT_BILLBOARD_DAILYDETAILSBUY&columns=ALL&filter=(TRADE_DATE%3D%27{0}%27)(SECURITY_CODE%3D%22{1}%22)&pageNumber=1&pageSize=50&sortTypes=-1&sortColumns=BUY".replace("{0}", date).replace("{1}", shareCode));
                rs = EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity());
                data = JSONObject.parseObject(rs);
                jsonArray = data.getJSONObject("result").getJSONArray("data");
                Integer size = jsonArray.size();
                //上榜原因
                String reason = jsonArray.getJSONObject(0).getString("EXPLANATION");
                for (Integer i = 0; i < size; i++) {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    buyer.append(String.valueOf(i + 1) + "、" + obj.getString("OPERATEDEPT_NAME")).append("\n");
                }
                buyer.deleteCharAt(buyer.length() - 1);
                httpGet = new HttpGet("https://datacenter-web.eastmoney.com/api/data/v1/get?reportName=RPT_BILLBOARD_DAILYDETAILSBUY&columns=ALL&filter=(TRADE_DATE%3D%27{0}%27)(SECURITY_CODE%3D%22{1}%22)&pageNumber=1&pageSize=50&sortTypes=-1&sortColumns=SELL".replace("{0}", date).replace("{1}", shareCode));
                rs = EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity());
                data = JSONObject.parseObject(rs);
                jsonArray = data.getJSONObject("result").getJSONArray("data");
                for (Integer i = 0; i < size; i++) {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    solder.append(String.valueOf(i + 1) + "、" + obj.getString("OPERATEDEPT_NAME")).append("\n");
                }
                solder.deleteCharAt(solder.length() - 1);
                stringBuilder.append("【最近一次上榜日期】").append(date).append("\n").append("【上榜原因】").append(reason).append("\n");
                stringBuilder.append("【买方席位】\n").append(buyer).append("\n").append("【卖方席位】\n").append(solder);
                String buyString = buyer.toString();
                String soldString = solder.toString();
                if (buyString.contains("机构") || soldString.contains("机构")) {
                    tag.add("机构");
                }
                if (buyString.contains("营业部") || buyString.contains("公司") || buyString.contains("单元") ||
                        soldString.contains("营业部") || soldString.contains("公司") || soldString.contains("单元")) {
                    tag.add("游资");
                }
                if (buyString.contains("沪股通") || soldString.contains("沪股通")) {
                    tag.add("沪股通");
                }
                if (soldString.contains("深股通") || soldString.contains("深股通")) {
                    tag.add("深股通");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        String rs = stringBuilder.toString();
        rs = rs.replaceAll("myTag", StringUtils.join(tag, "、"));
        return rs;
    }

    public String queryEtfSummary(String shareCode) {
        HttpGet httpGet = null;
        Double sum = 0.0;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            httpGet = new HttpGet(String.format("http://%s/etf_summary?code=%s", ipAndPort, shareCode));
            httpGet.setConfig(RequestConfig.custom().setConnectTimeout(3000).build());
            JSONObject jsonObject = JSON.parseObject(EntityUtils.toString(httpClient.execute(httpGet).getEntity()));
            stringBuilder.append(String.format("1、代码：%s\n", shareCode));
            stringBuilder.append(String.format("2、全称：%s\n", jsonObject.getString("short_name")));
            stringBuilder.append(String.format("3、规模：%s\n", jsonObject.getString("scale")));
            stringBuilder.append(String.format("4、跟踪标的：%s\n", jsonObject.getString("follow")));
            if (StringUtils.isNotBlank(jsonObject.getString("RZYE"))) {
                stringBuilder.append(String.format("5、融资余额：%.3f亿元，融券余额：%.3f亿元\n", jsonObject.getDouble("RZYE") / 1e8, jsonObject.getDouble("RQYE") / 1e8));
            } else {
                stringBuilder.append("5、融资余额：非标的，融券余额：非标的\n");
            }
            stringBuilder.append("${1}");
            stringBuilder.append(String.format("---------------------------->持仓股<----------------------------\n"));
            JSONArray jsonArray = jsonObject.getJSONArray("hold_list");
            Integer size = jsonArray.size();
            for (Integer idx = 0; idx < size; idx++) {
                JSONObject item = jsonArray.getJSONObject(idx);
                String percent = item.getString("percent");
                sum += Double.valueOf(percent.substring(0, percent.length() - 1));
                stringBuilder.append(String.format("%d、%s，%s，%s，%+.2f%%\n", idx + 1, item.getString("code"), item.getString("name"),
                        percent, item.getDouble("change")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
        return stringBuilder.toString().replace("${1}", String.format("5、公开持仓占比总计：%.2f%%\n", sum));
    }

    public void query(String text) {
        String shareCode = null;
        Boolean isEastFutureCode = isEastFutureCode(text);
        Matcher m = FollowUtils.STOCK_CODE_PATTERN.matcher(text);
        if (m.find() || isEastFutureCode) {
            shareCode = text;
        } else {
            HttpGet httpGet = null;
            try {
                httpGet = new HttpGet(String.format("https://suggest3.sinajs.cn/suggest/?key=%s", text));
                httpGet.addHeader("Referer", "https://finance.sina.com.cn");
                httpGet.setConfig(RequestConfig.custom().setConnectTimeout(1000).build());
                String rs = EntityUtils.toString(httpClient.execute(httpGet).getEntity());
                shareCode = rs.split(",")[2];
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (Objects.nonNull(httpGet)) {
                    httpGet.releaseConnection();
                }
            }
        }
        if (isEastFutureCode) {
            jEditorPane.setText(queryFutureSummary(shareCode));
        } else if (shareCode.startsWith("1") || shareCode.startsWith("5")) {
            jEditorPane.setText(queryEtfSummary(shareCode));
        } else {
            jEditorPane.setText(querySummary(shareCode, httpClient, jEditorPane, ipAndPort));
        }
        jEditorPane.setCaretPosition(0);
        textField1.setText(null);
        setVisible(true);
    }

    private Boolean isEastFutureCode(String code) {
        if (code.contains(".")) {
            return true;
        } else {
            return false;
        }
    }
}

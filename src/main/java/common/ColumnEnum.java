package common;

public enum ColumnEnum {

    TABLE_NO(0, "NO"),
    TABLE_MC(1, "MC"),
    TABLE_JG(2, "JG"),
    TABLE_ZDF(3, "ZDF"),
    TABLE_V20(4, "V20"),
    TABLE_G10(5, "G10"),
    TABLE_G20(6, "G20"),
    TABLE_QJZDF(7, "QJZDF"),
    TABLE_BJ(8, "BJ"),
    TABLE_MACD(9, "MACD"),
    TABLE_JLR(10, "JLR"),
    TABLE_SYZ(11, "SYZ"),
    TABLE_XYZ(12, "XYZ"),
    TABLE_GJ(13, "GJ"),
    TABLE_BZ(14, "BZ");

    private Integer code;

    private String desc;

    ColumnEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}

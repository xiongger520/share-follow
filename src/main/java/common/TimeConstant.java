package common;

import java.time.format.DateTimeFormatter;

public class TimeConstant {
    public static String YYYY_MM_DD_SPLIT = "yyyy-MM-dd";
    public static String YYYY_MM_DD_NO_SPLIT = "yyyyMMdd";
    public static DateTimeFormatter DATE_FORMATTER_Y_M_D_SPLIT = DateTimeFormatter.ofPattern(YYYY_MM_DD_SPLIT);
    public static DateTimeFormatter DATE_FORMATTER_Y_M_D_NO_SPLIT = DateTimeFormatter.ofPattern(YYYY_MM_DD_NO_SPLIT);
    public static DateTimeFormatter DATE_FORMATTER_Y_M_D_CN = DateTimeFormatter.ofPattern("yyyy年M月d日");
    public static DateTimeFormatter DATE_TIME_FORMATTER_Y_M_D_H_M_S = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static DateTimeFormatter DATE_FORMATTER_Y_CN = DateTimeFormatter.ofPattern("yyyy年");
    public static DateTimeFormatter TIME_FORMATTER_H_M_S = DateTimeFormatter.ofPattern("HH:mm:ss");
}

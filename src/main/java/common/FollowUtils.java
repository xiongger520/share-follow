package common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import javax.imageio.ImageIO;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.*;
import java.util.regex.Pattern;

public class FollowUtils {

    public static Pattern STOCK_CODE_PATTERN = Pattern.compile("^\\d{6}$");
    public static Pattern FUTURE_MONTH_PATTERN = Pattern.compile("\\d{1,4}");
    public static Pattern Y_M_D_CN_PATTERN = Pattern.compile("\\d{4}年\\d{1,2}月\\d{1,2}日");
    public static String[] KLINE_LEVEL = {"日线", "周线", "日周线", "月线", "日周月线"};
    public static String[] KLINE_LEVEL_EN = {"day", "week", "dayWeek", "month", "dayWeekMonth"};
    public static Map<String, String> COMBOBOX_MAP = new LinkedHashMap<String, String>() {{
        put("全All", null);
        put("回踩0~3%", "dw,^[0-2]\\.\\d\\d%$");
        put("周线回踩", "w,^[0-2]\\.\\d\\d%$");
        put("回踩0~6%", "dw,^[0-5]\\.\\d\\d%$");
        put("反抽-3~0%", "13,^-[0-2]\\.\\d\\d%$");
        put("反抽-6~0%", "13,^-[0-5]\\.\\d\\d%$");
        put("日线多头d", "d");
        put("周线多头w", "w");
        put("月线多头m", "m");
        put("日周线多头dw", "dw");
        put("周月线多头wm", "wm");
        put("日周月多头dwm", "dwm");
        put("周线G20", "");
        put("月线G20", "");
        put("日线空头1", "1");
        put("周线空头3", "3");
        put("月线空头5", "5");
        put("日周线空头13", "13");
        put("周月线空头35", "35");
        put("日周月空头135", "135");
        put("分钟模式", "");
    }};

    //查询昨天和今天（前一个交易日、最近一个交易日）
    public static ImmutablePair<LocalDate, LocalDate> queryYesterdayAndToday(CloseableHttpClient closeableHttpClient, Boolean futureMode) {
        HttpGet httpGet = null;
        try {
            httpGet = new HttpGet();
            if (futureMode) {
                httpGet.setURI(URI.create("http://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f51&klt=101&fqt=1&secid=113.aum&beg=0&end=20500000"));
            } else {
                httpGet.setURI(URI.create("http://push2his.eastmoney.com/api/qt/stock/kline/get?fields1=f1&fields2=f51&klt=101&fqt=1&secid=1.000001&beg=0&end=20500000"));
            }
            JSONArray jsonArray = JSON.parseObject(EntityUtils.toString(closeableHttpClient.execute(httpGet).getEntity())).getJSONObject("data").getJSONArray("klines");
            Integer size = jsonArray.size();
            return ImmutablePair.of(LocalDate.parse(jsonArray.getString(size - 2)), LocalDate.parse(jsonArray.getString(size - 1)));
        } catch (Exception e1) {
            e1.printStackTrace();
            return null;
        } finally {
            if (Objects.nonNull(httpGet)) {
                httpGet.releaseConnection();
            }
        }
    }

    public static TableRowSorter<TableModel> getColumnSorters(DefaultTableModel defaultTableModel) {
        TableRowSorter<TableModel> tableRowSorter = new TableRowSorter<>(defaultTableModel);
        tableRowSorter.setSortable(ColumnEnum.TABLE_SYZ.getCode(), false);
        tableRowSorter.setSortable(ColumnEnum.TABLE_XYZ.getCode(), false);
        //点击序号时按照数字进行排序
        tableRowSorter.setComparator(ColumnEnum.TABLE_NO.getCode(), new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                return commonCompare(o1, o2);
            }
        });
        tableRowSorter.setComparator(ColumnEnum.TABLE_JG.getCode(), new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                return commonCompare(o1, o2);
            }
        });
        tableRowSorter.setComparator(ColumnEnum.TABLE_ZDF.getCode(), new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                return percentCompare(o1, o2);
            }
        });
        tableRowSorter.setComparator(ColumnEnum.TABLE_V20.getCode(), new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                return percentCompare(o1, o2);
            }
        });
        tableRowSorter.setComparator(ColumnEnum.TABLE_G10.getCode(), new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                return percentCompare(o1, o2);
            }
        });
        tableRowSorter.setComparator(ColumnEnum.TABLE_G20.getCode(), new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                return percentCompare(o1, o2);
            }
        });
        tableRowSorter.setComparator(ColumnEnum.TABLE_JLR.getCode(), new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                return commonCompare(o1, o2);
            }
        });
        tableRowSorter.setComparator(ColumnEnum.TABLE_QJZDF.getCode(), new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                return percentCompare(o1, o2);
            }
        });
        return tableRowSorter;
    }

    //普通比较（无百分号）,null不参与比较
    private static int commonCompare(Object o1, Object o2) {
        Double num1 = Double.valueOf(o1.toString());
        Double num2 = Double.valueOf(o2.toString());
        if (num1 < num2) {
            return -1;
        } else if (num1 > num2) {
            return 1;
        } else {
            return 0;
        }
    }

    //百分号比较,null不参与比较
    private static int percentCompare(Object o1, Object o2) {
        String str1 = o1.toString();
        String str2 = o2.toString();
        Double num1 = Double.valueOf(str1.substring(0, str1.length() - 1));
        Double num2 = Double.valueOf(str2.substring(0, str2.length() - 1));
        if (num1 < num2) {
            return -1;
        } else if (num1 > num2) {
            return 1;
        } else {
            return 0;
        }
    }

    //睡眠
    public static void mySleep(Integer millisSeconds) {
        try {
            Thread.sleep(millisSeconds);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    public static void mergeImage(List<InputStream> inputStreamList, File file, Boolean isHorizontal) {
        try {
            List<BufferedImage> sourceList = new ArrayList<>();
            for (InputStream inputStream : inputStreamList) {
                sourceList.add(ImageIO.read(inputStream));
            }
            // 生成新图片
            BufferedImage target;
            // 计算新图片的长和高
            int allWidth = 0, allHeight = 0, allWidthMax = 0, allHeightMax = 0;
            // 获取总长、总宽、最长、最宽
            for (int i = 0; i < sourceList.size(); i++) {
                BufferedImage img = sourceList.get(i);
                allWidth += img.getWidth();
                if (sourceList.size() != i + 1) {
                    allHeight += img.getHeight() + 2;
                } else {
                    allHeight += img.getHeight();
                }
                if (img.getWidth() > allWidthMax) {
                    allWidthMax = img.getWidth();
                }
                if (img.getHeight() > allHeightMax) {
                    allHeightMax = img.getHeight();
                }
            }
            // 创建新图片
            if (isHorizontal) {
                target = new BufferedImage(allWidth, allHeightMax, BufferedImage.TYPE_INT_RGB);
            } else {
                target = new BufferedImage(allWidthMax, allHeight, BufferedImage.TYPE_INT_RGB);
            }
            // 合并所有子图片到新图片
            int wx = 0, wy = 0;
            for (BufferedImage img : sourceList) {
                int w1 = img.getWidth();
                int h1 = img.getHeight();
                // 从图片中读取RGB
                int[] imageArrayOne = new int[w1 * h1];
                // 逐行扫描图像中各个像素的RGB到数组中
                imageArrayOne = img.getRGB(0, 0, w1, h1, imageArrayOne, 0, w1);
                if (isHorizontal) {
                    // 水平方向合并
                    // 设置上半部分或左半部分的RGB
                    target.setRGB(wx, 0, w1, h1, imageArrayOne, 0, w1);
                } else {
                    // 垂直方向合并
                    // 设置上半部分或左半部分的RGB
                    target.setRGB(0, wy, w1, h1, imageArrayOne, 0, w1);
                }
                //空白部分填充白色
                if (!isHorizontal && w1 < allWidthMax) {
                    Color color = new Color(255, 255, 255);
                    int rgb = color.getRGB();
                    for (int x = w1; x < allWidthMax; x++) {
                        for (int y = wy; y < wy + h1; y++) {
                            target.setRGB(x, y, rgb);
                        }
                    }
                }
                wx += w1;
                wy += h1 + 2;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            //输出想要的图片
            ImageIO.write(target, "png", fileOutputStream);
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
